﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour {

    [SerializeField]
    private GameObject main;

    [SerializeField]
    private GameObject options;

    [SerializeField]
    private GameObject controls;

    private GameObject mainCamera;

    private void Awake() {
        if (main == null) {
            main = GameObject.Find("Main");
            if (main == null) enabled = false;
            return;
        }
        if (options == null) {
            options = GameObject.Find("Options");
            if (options == null) enabled = false;
            return;
        }
        if (controls == null) {
            controls = GameObject.Find("Controls");
            if (controls == null) enabled = false;
            return;
        }

        mainCamera = GameObject.FindGameObjectWithTag("MainCamera");
    }

    private void Start() {
        int enableGeneticAlgorithm = PlayerPrefs.GetInt("GeneticAlgorithm", 0);
        float musicVolume = PlayerPrefs.GetFloat("MusicVolume", 0.5f);
        mainCamera.GetComponent<AudioSource>().volume = musicVolume;

        options.GetComponentInChildren<Toggle>().isOn = enableGeneticAlgorithm == 1 ? true : false;
        options.GetComponentInChildren<Slider>().value = musicVolume;
    }

    public void OnStart() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void OnQuit() {
        Application.Quit();
    }

    public void OnControls() {
        main.SetActive(false);
        controls.SetActive(true);
    }

    public void OnOptions() {
        main.SetActive(false);
        options.SetActive(true);
    }

    public void OnMusicVolumeChanged(Slider slider) {
        PlayerPrefs.SetFloat("MusicVolume", slider.value);
        mainCamera.GetComponent<AudioSource>().volume = slider.value;
    }

    public void OnGeneticAlgorithmChanged(Toggle toggle) {
        PlayerPrefs.SetInt("GeneticAlgorithm", toggle.isOn ? 1 : 0);
    }

    public void OnBack() {
        controls.SetActive(false);
        options.SetActive(false);
        main.SetActive(true);
    }
}
