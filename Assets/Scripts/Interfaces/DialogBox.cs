﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogBox : MonoBehaviour {

    public static DialogBox instance;

    private Image dialogFrame;
    private Text text;

    private void Awake() {
        instance = this;

        dialogFrame = GetComponent<Image>();
        text = GetComponentInChildren<Text>();
    }

    private void Start() {
        Hide();
    }

    public static void Show(string displayText) {
        instance.DoShow(displayText);
    }

    public static void Hide() {
        instance.DoHide();
    }

    public static bool IsVisible() {
        return instance.dialogFrame.enabled;
    }

    public static string GetText() {
        return instance.text.text;
    }

    private void DoHide() {
        dialogFrame.enabled = false;
        text.enabled = false;
    }

    private void DoShow(string displayText) {
        dialogFrame.enabled = true;


        text.enabled = true;
        text.text = displayText;
    }
}
