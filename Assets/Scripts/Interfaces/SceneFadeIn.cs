﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class SceneFadeIn : MonoBehaviour {

    [SerializeField]
    private float timeBeforeFadeIn = 0.5f;

    [SerializeField]
    private float fadeInRate = 50f;

    private Image fadeImage;
    private bool beginFadeIn = false;

    private void Awake() {
        fadeImage = GetComponent<Image>();
    }

    private void Start() {
        Invoke("StartFadeIn", timeBeforeFadeIn);
    }

    private void StartFadeIn() {
        beginFadeIn = true;
    }

    // Update is called once per frame
    void Update () {
        if (!beginFadeIn) return;
        float newOpacity = fadeImage.color.a - (fadeInRate / 100f) * Time.deltaTime;
        if (newOpacity <= 0) {
            Destroy(gameObject);
            return;
        }
        fadeImage.color = new Color(0, 0, 0, newOpacity);
	}
}
