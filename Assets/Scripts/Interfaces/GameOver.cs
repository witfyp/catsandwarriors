﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour {

	public void OnContinue() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void OnQuit() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex-1);
    }
}
