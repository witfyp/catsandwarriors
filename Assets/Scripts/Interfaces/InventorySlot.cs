﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InventorySlot : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {

    [SerializeField]
    private Sprite emptySlotSprite;

    public Image icon;
    public Button button;

    private ItemBase item;
    private RectTransform slotRect;

    public ItemBase Item { get { return item; } }

    private void Awake() {
        slotRect = GetComponent<RectTransform>();
    }

    void Start () {
        if (icon == null) {
            Debug.LogError("Inventory Slot Does Not Have Reference To Icon Image!");
            enabled = false;
        }
        if (button == null) {
            Debug.LogError("Inventory Slot Does Not Have Reference To Button!");
            enabled = false;
        }
	}
	
	public void AddItem(ItemBase newItem) {
        item = newItem;
        icon.sprite = item.sprite;
    }

    public void ClearSlot() {
        item = null;
        icon.sprite = emptySlotSprite;
        Tooltip.Hide();
    }

    public void OnPointerEnter(PointerEventData eventData) {
        if (item == null) return;
        Tooltip.Show(slotRect.anchoredPosition.y, item.itemName, item.itemDescription, item.itemNameColour);
    }

    public void OnPointerExit(PointerEventData eventData) {
        Tooltip.Hide();
    }
}
