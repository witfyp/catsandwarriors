﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum MusicTheme { VILLAGE, FOREST }

public class AudioManager : MonoBehaviour {

    public static AudioManager instance;

    [SerializeField]
    private AudioSource audioSource;

    [System.Serializable]
    private struct MusicThemeStruct {
        public MusicTheme theme;
        public AudioClip clip;
    }

    [SerializeField]
    private List<MusicThemeStruct> music;

    private void Awake() {
        instance = this;
        if (audioSource == null) {
            Debug.LogError("Could not find audio source!");
            enabled = false;
        }
    }

    private void Start() {
        audioSource.clip = GetMusicClip(MusicTheme.VILLAGE);
        audioSource.volume = PlayerPrefs.GetFloat("MusicVolume", 0.5f);
        audioSource.Play();
    }

    public static void SetMusicVolume(Slider slider) {
        PlayerPrefs.SetFloat("MusicVolume", slider.value);
        instance.audioSource.volume = slider.value;
    }

    public static void SetMusicVolume(float value) {
        PlayerPrefs.SetFloat("MusicVolume", value);
        instance.audioSource.volume = value;
    }

    public static void SetMusicClip(AudioClip clip) {
        if (instance.audioSource.clip == clip) return;
        instance.audioSource.clip = clip;
        instance.audioSource.Play();
    }

    public static void SetMusicClip(MusicTheme theme) {
        AudioClip clip = GetMusicClip(theme);
        SetMusicClip(clip);
    }

    public static AudioClip GetMusicClip (MusicTheme theme) {
        foreach(MusicThemeStruct m in instance.music) {
            if (theme == m.theme) return m.clip;
        }
        return null;
    }
}
