﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicTrigger : MonoBehaviour {

    [SerializeField]
    private MusicTheme changeTo;

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.tag != "Player") return;
        AudioManager.SetMusicClip(changeTo);
    }
}
