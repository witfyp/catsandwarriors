﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System;
using UnityEngine.EventSystems;

public class ChatInputBox : MonoBehaviour {

    private static ChatInputBox instance;

    private InputField input;

    private void Awake() {
        instance = this;
        input = GetComponent<InputField>();
        DoHide();
    }

    public static void Show(Action<string> inputcallback) {
        instance.DoShow(inputcallback);
    }

    public static void Hide() {
        instance.DoHide();
    }

    public static bool IsVisible() {
        return instance.gameObject.activeInHierarchy;
    }

    private void DoHide() {
        input.onEndEdit.RemoveAllListeners();
        gameObject.SetActive(false);
    }

    private void DoShow(Action<string> inputcallback) {
        gameObject.SetActive(true);
        input.onEndEdit.AddListener(delegate { inputcallback(input.text); });
    }
}
