﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChoiceBox : MonoBehaviour {

    // Inspector field
    [SerializeField]
    private GameObject buttonTemplate;

    private static ChoiceBox instance;

    private List<Button> choiceButtons;
    private int selectedIndex;

    private void Start() {
        instance = this;

        choiceButtons = new List<Button>();
        selectedIndex = 0;
    }

    public static void Show(List<ChoiceBase> choices, Character character) {
        instance.DoShow(choices, character);
    }

    public static void Hide() {
        instance.DoHide();
    }

    public static void HightlightChoice(bool selectedUp) {
        instance.DoHightlight(selectedUp);
    }

    public static bool IsVisible() {
        return instance.choiceButtons.Count > 0;
    }

    public static int GetSelectedChoice() {
        return instance.selectedIndex;
    }

    private void DoHide() {

        foreach (Transform child in transform) {
            Destroy(child.gameObject);
        }

        choiceButtons.Clear();
        selectedIndex = 0;
    }

    private void DoShow(List<ChoiceBase> choices, Character character) {
        DoHide();
        foreach (ChoiceBase choice in choices) {
            Button b = Instantiate(buttonTemplate, transform).GetComponent<Button>();
            b.GetComponentInChildren<Text>().text = choice.ChoiceName;
            b.onClick.AddListener(delegate { choice.OnExecute(character); });
            choiceButtons.Add(b);
        }
    }

    private void DoHightlight(bool selectedUp) {

        if (choiceButtons.Count == 0) return;

        if (selectedUp && instance.selectedIndex > 0) instance.selectedIndex--;
        else if (!selectedUp && instance.selectedIndex < instance.choiceButtons.Count - 1) instance.selectedIndex++;

        instance.choiceButtons[instance.selectedIndex].Select();
    }
}
