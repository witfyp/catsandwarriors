﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameMenu : MonoBehaviour {

    public static GameMenu instance;

    private GameObject main;
    private GameObject options;
    private GameObject player;
    private GameObject mainCamera;

    private void Awake() {
        instance = this;
        main = transform.Find("Main").gameObject;
        options = transform.Find("Options").gameObject;
        player = GameObject.FindGameObjectWithTag("Player");
        mainCamera = GameObject.FindGameObjectWithTag("MainCamera");
    }

    private void Start() {
        instance.gameObject.SetActive(false);
    }

    public static void OnMenuPressed() {
        instance.OnBack();
        instance.player.GetComponent<Character>().Movement.Frozen = !instance.gameObject.activeInHierarchy;
        Time.timeScale = instance.gameObject.activeInHierarchy ? 1 : 0;
        instance.gameObject.SetActive(!instance.gameObject.activeInHierarchy);
    }

    public void OnContinue() {
        OnMenuPressed();
    }

    public void OnOptions() {
        main.SetActive(false);
        options.SetActive(true);
    }

    public void OnMusicVolumeChanged(Slider slider) {
        PlayerPrefs.SetFloat("MusicVolume", slider.value);
        mainCamera.GetComponent<AudioSource>().volume = slider.value;
    }

    public void OnGeneticAlgorithmChanged(Toggle toggle) {
        PlayerPrefs.SetInt("GeneticAlgorithm", toggle.isOn ? 1 : 0);
    }

    public void OnBack() {
        options.SetActive(false);
        main.SetActive(true);
    }

    public void OnQuit() {
        Application.Quit();
    }
}
