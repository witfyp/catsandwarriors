﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpeechBubble : MonoBehaviour {

    [SerializeField]
    private Image bubbleImage;

    [SerializeField]
    private Text text;

    [SerializeField]
    private float durationPerMessage = 2f;

    private float timer;

    private void Start() {
        timer = 0f;
        Hide();
    }

    public bool IsVisible() {
        return bubbleImage.enabled && text.enabled;
    }

    private void Hide() {
        text.text = "";
        text.enabled = false;
        timer = 0;
        bubbleImage.enabled = false;
    }

    public void Show(string message) {
        text.enabled = true;
        bubbleImage.enabled = true;
        text.text = message;
    }

    private void Update() {
        if (!IsVisible()) return;
        timer += Time.deltaTime;
        if (timer >= durationPerMessage) Hide();
    }
}
