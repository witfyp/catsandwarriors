﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ClickableObjectHelper : MonoBehaviour, IPointerClickHandler {

    public delegate void OnClickEvent(InventorySlot slot);
    public OnClickEvent onLeftClickEventCallback;
    public OnClickEvent onRightClickEventCallback;
    public OnClickEvent onMiddleClickEventCallback;

    public void OnPointerClick(PointerEventData eventData) {
        if (eventData.button == PointerEventData.InputButton.Left) {
            if (onLeftClickEventCallback != null) onLeftClickEventCallback.Invoke(GetComponent<InventorySlot>());
        } else if (eventData.button == PointerEventData.InputButton.Right) {
            if (onRightClickEventCallback != null) onRightClickEventCallback.Invoke(GetComponent<InventorySlot>());
        } else if (eventData.button == PointerEventData.InputButton.Right) {
            if (onMiddleClickEventCallback != null) onMiddleClickEventCallback.Invoke(GetComponent<InventorySlot>());
        }
    }
}
