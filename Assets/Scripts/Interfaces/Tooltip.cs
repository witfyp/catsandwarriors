﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tooltip : MonoBehaviour {

    private static Tooltip instance;

    [SerializeField]
    private Text nameText;
    [SerializeField]
    private Text descriptionText;

    private RectTransform tooltipRect;

    private void Awake() {
        instance = this;
        tooltipRect = GetComponent<RectTransform>();
        if (nameText == null) {
            Debug.LogError("Tooltip script does not have a reference to name text component!");
            enabled = false;
        }
        if (descriptionText == null) {
            Debug.LogError("Tooltip script does not have a reference to description text component!");
            enabled = false;
        }
        DoHide();
    }

    public static void Show(float y ,string name, string description, Color colour) {
        if (name == null || name == "") return;
        if (description == null) description = "";
        instance.DoShow(y, name, description, colour);
    }

    public static void Hide() {
        instance.DoHide();
    }

    public static bool IsVisible() {
        return instance.gameObject.activeInHierarchy;
    }

    private void DoHide() {
        nameText.text = "";
        descriptionText.text = "";
        gameObject.SetActive(false);
    }

    private void DoShow(float y, string name, string description, Color colour) {
        gameObject.SetActive(true);
        nameText.text = name;
        nameText.color = colour;
        descriptionText.text = description;
        tooltipRect.anchoredPosition = new Vector2(tooltipRect.anchoredPosition.x, y);
    }
}
