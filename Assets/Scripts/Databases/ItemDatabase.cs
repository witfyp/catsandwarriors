﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemDatabase : ScriptableObject {

    [SerializeField]
    private ItemBase[] data;

    public void Awake() {
        data = Resources.LoadAll<ItemBase>("Items");
    }

    public ItemBase FindItem(ItemType itemType) {
        foreach(ItemBase item in data) {
            if (item.type == itemType) return item;
        }
        return null;
    }
}
