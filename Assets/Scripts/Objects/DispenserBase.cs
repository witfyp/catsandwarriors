﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DispenserBase : MonoBehaviour {

    [SerializeField]
    private float amount;
    [SerializeField]
    private float capacity = 100f;
    [SerializeField]
    [Range(1, 100)]
    private float lowPointPercentage = 25f;

    public bool Empty { get { return amount <= 0; } }
    public float Capacity { get { return capacity; } }
    public float Amount { get { return amount; } }
    public float LowPoint { get { return (amount/capacity) * lowPointPercentage; } }

    public virtual float Take(float n) {
        if (n > amount) {
            n = amount;
            amount = 0;
        } else {
            amount -= n;
        }
        return n;
    }

    public virtual void Put(float n) {
        amount += n;
        if (amount > capacity) amount = capacity;
    }
}
