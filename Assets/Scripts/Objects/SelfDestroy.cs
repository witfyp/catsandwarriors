﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfDestroy : MonoBehaviour {

    public float timer = 3f;

	// Use this for initialization
	private void Start () {
        Destroy(gameObject, timer);
	}
}
