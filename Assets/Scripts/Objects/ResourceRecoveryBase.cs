﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceRecoveryBase : MonoBehaviour {

    [SerializeField]
    private float recoveryTime = 120f;
    [SerializeField]
    private GameObject recoveryEffect;
    [SerializeField]
    private GameObject recoverWith;

    private float timer;

	// Use this for initialization
	void Start () {
        timer = 0;
        if (recoverWith == null) Debug.LogWarning("Resource recovery gameobject is not set!", gameObject);
	}

    private void OnDestroy() {
        PathRequestManager.UpdateGrid(gameObject, gameObject.transform.position);
    }

    // Update is called once per frame
    void Update () {
        timer += Time.deltaTime;
        if (timer < recoveryTime) return;
        if (recoveryEffect != null) Instantiate(recoveryEffect, transform.position, Quaternion.identity);
        Instantiate(recoverWith, transform.position, Quaternion.identity);
        Destroy(gameObject);
	}
}
