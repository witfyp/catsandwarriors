﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceBase : MonoBehaviour {

    [SerializeField]
    private ItemType resourceType;
    [SerializeField]
    private float amount = 7;
    [SerializeField]
    private GameObject onDepletedEffect;
    [SerializeField]
    private GameObject onDepletedReplaceWith;

    public ItemType Resource { get { return resourceType; } }

    public bool CanTake { get { return amount > 0; } }

    public virtual ItemBase Take() {
        if (!CanTake) return null;
        amount--;
        return Database.Item.FindItem(resourceType);
    }

    public virtual void Refund() {
        amount++;
    }

    private void OnDestroy() {
        PathRequestManager.UpdateGrid(gameObject, gameObject.transform.position);
    }

    private void Update () {
        if (amount > 0) return;
        if (onDepletedEffect != null) Instantiate(onDepletedEffect, transform.position, Quaternion.identity);
        if (onDepletedReplaceWith != null) Instantiate(onDepletedReplaceWith, transform.position, Quaternion.identity);
        Destroy(gameObject);
	}
}
