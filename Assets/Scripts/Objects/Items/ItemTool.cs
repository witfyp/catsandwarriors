﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Tool Item", menuName = "Inventory/Tool")]
public class ItemTool : ItemBase {

    public float durability;
    public float decayRate;
    public List<ItemType> usedOn;

    public override bool UseOn(Character character, GameObject obj) {
        if (character.Tool == null) {
            Debug.Log("Character Does Not Have Tool Model To Use A Tool");
            return false;
        }
        ResourceBase resource = obj.GetComponent<ResourceBase>();
        if (resource == null) {
            Debug.Log("Object To Act Upon Does Not Have ResouceBase Script");
            return false;
        }
        if (!usedOn.Contains(resource.Resource)) {
            Debug.Log("Tool " + type.ToString() + " Cannot Be Used On Type " + resource.Resource.ToString());
            return false;
        }
        if(character.Inventory == null) {
            Debug.Log("Character Does Not Have Inventory Model To Take Resource");
            return false;
        }

        if (!character.Inventory.AddItem(resource.Take())) {
            resource.Refund();
            return false; //Cannot take resource into inventory;
        }

        character.Tool.DecreaseToolDurability(decayRate);
        return true;
    }
}
