﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickableItem : MonoBehaviour {

    [SerializeField]
    private ItemType type;

    private void OnTriggerEnter2D(Collider2D collision) {
        CharacterInventoryModel inventory = collision.GetComponent<CharacterInventoryModel>();

        if (inventory == null) return;
        if (inventory.AddItem(Database.Item.FindItem(type))) Destroy(gameObject);
    }
}
