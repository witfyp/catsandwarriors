﻿public enum ItemType {
    NONE = 0,


    //Resources
    WOOD = 10,
    WOOD_LARGE,

    ORE = 20,
    ORE_COPPER,
    ORE_IRON,
    ORE_SILVER,
    ORE_GOLD,

    HERB = 30,


    // Potions
    HEALTH_POTION = 50,

    SPEED_POTION = 60,

    ATTACK_POTION = 70,


    // Tools
    AXE = 80,

    PICK_AXE = 90,

    SCISSORS = 100
}
