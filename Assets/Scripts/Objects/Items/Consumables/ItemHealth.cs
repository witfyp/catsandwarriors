﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Health Item", menuName = "Inventory/Consumables/Health Item")]
public class ItemHealth : ItemBase {

    public float healingAmount;

    public override bool Use(Character character) {
        if (character.Health == null) return false;
        character.Health.IncreaseValue(character.Health.MaxValue * (healingAmount/100f));
        if (character.Inventory != null) character.Inventory.RemoveItem(this);
        return true;
    }
}
