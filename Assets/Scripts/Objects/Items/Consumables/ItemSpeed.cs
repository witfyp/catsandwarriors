﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Speed Item", menuName = "Inventory/Consumables/Speed Item")]
public class ItemSpeed : ItemBase {

    public float speedMultiplier;
    public float duration;

    public override bool Use(Character character) {
        if (character.Movement == null) return false;
        character.Movement.SetSpeedMultiplier(speedMultiplier, duration);
        if (character.Inventory != null) character.Inventory.RemoveItem(this);
        return true;
    }
}
