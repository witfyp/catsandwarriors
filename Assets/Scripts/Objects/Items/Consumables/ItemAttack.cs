﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Attack Item", menuName = "Inventory/Consumables/Attack Item")]
public class ItemAttack : ItemBase {

    public float attackMultiplier;
    public float duration;

    public override bool Use(Character character) {
        if (character.Attack == null) return false;
        character.Attack.SetAttackMultiplier(attackMultiplier, duration);
        if (character.Inventory != null) character.Inventory.RemoveItem(this);
        return true;
    }
}
