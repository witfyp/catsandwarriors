﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Item", menuName = "Inventory/Item")]
public class ItemBase : ScriptableObject {

    public string itemName;
    [TextArea(1, 3)]
    public string itemDescription;
    public Color itemNameColour = Color.yellow;

    public ItemType type;
    public Sprite sprite;
    public GameObject prefab;

    public virtual bool Use(Character character) {
        Debug.Log("Using Item " + type.ToString());
        return true;
    }

    public virtual bool UseOn(Character character, GameObject obj) {
        Debug.Log("Using Item " + type.ToString() + " on " + obj.name);
        return true;
    }
}
