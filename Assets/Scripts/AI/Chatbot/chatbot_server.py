import chatbot
import zmq

context = zmq.Context()
socket = context.socket(zmq.REP)
socket.bind("tcp://*:5555")

chatbot.setup_chatbot()

while True:
    try:
        query = socket.recv(flags=zmq.NOBLOCK).decode("utf-8") 
        if (query == "<QUIT_CHATBOT>"):
            raise KeyboardInterrupt()
        print("Received request: %s" % query)
        response = chatbot.chat(query)
        print("Response: %s\n" % response)
        socket.send(response.encode('utf-8'))
    except zmq.Again as e:
        continue
    except KeyboardInterrupt:
        print("EXTING CHATBOT - SEVER CLOSING")
        socket.close()
        context.term()
        break