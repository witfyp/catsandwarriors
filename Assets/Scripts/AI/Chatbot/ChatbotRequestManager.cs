﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Threading;
using System.Diagnostics;

/// <summary>
/// <para>Class <c>ChatbotRequestManager</c> is singleton which handles requests to the chatbot.</para>
/// 
/// <para>
/// AUTHOR: Oleksandr Kononov
/// </para>
/// </summary>
[RequireComponent(typeof(Chatbot))]
public class ChatbotRequestManager : MonoBehaviour {

    [SerializeField]
    private bool hideDebugWindow = true;

    // Singleton instance of the class
    public static ChatbotRequestManager instance;

    // Access to the chatbot class
    private Chatbot chatbot;

    // Chatbot process
    private Process chatbotProcess;
    private string chatbotProcessName = "";

    // Boolean to check if the chatbot server is running
    private static bool Available { get { return Process.GetProcessesByName(instance.chatbotProcessName).Length > 0; } }

    // Opening lines for chats
    private static string[] openingLines;

    // Queue of results from chatbot requests
    private Queue<ChatbotResult> results = new Queue<ChatbotResult>();

    /// <summary>
    /// Awake runs once at the initialisation of the scene stage and
    /// before any other functions (with the exception of other Awake functions).
    /// 
    /// <para>Used for initialisation singleton class and chatbot process.</para>
    /// </summary>
    private void Awake() {
        instance = this;
        chatbot = GetComponent<Chatbot>();
        openingLines = JsonHelper.getJsonArray<string>(Resources.Load<TextAsset>("Chatbot/openingLines").text);

        chatbotProcess = new Process();
        chatbotProcess.StartInfo.FileName = "python";
        chatbotProcess.StartInfo.Arguments = "chatbot_server.py";
        if (hideDebugWindow) chatbotProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
        if (Application.isEditor) chatbotProcess.StartInfo.WorkingDirectory = Application.dataPath + "/Scripts/AI/Chatbot";
        else chatbotProcess.StartInfo.WorkingDirectory = "./CatsAndWarriors_Data/Chatbot/";
        if (chatbotProcess.Start()) chatbotProcessName = chatbotProcess.ProcessName;
    }

    /// <summary>
    /// Gets opening lines for chats.
    /// </summary>
    /// <returns>Primitive array of string of opening lines.</returns>
    public static string[] GetOpeningLines() {
        if (openingLines == null) openingLines = JsonHelper.getJsonArray<string>(Resources.Load<TextAsset>("Chatbot/openingLines").text);
        return openingLines;
    }

    /// <summary>
    /// Get a random opening line for chats.
    /// </summary>
    /// <returns>String of an opening line.</returns>
    public static string GetRandomOpeningLine() {
        if (openingLines == null) openingLines = JsonHelper.getJsonArray<string>(Resources.Load<TextAsset>("Chatbot/openingLines").text);
        return openingLines[UnityEngine.Random.Range(0, openingLines.Length)];
    }

    /// <summary>
    /// Create a chat request for the chatbot.
    /// </summary>
    /// <param name="request">Chatbot request structure.</param>
    public static void Request(ChatbotRequest request) {
        if (!Available) request.callback?.Invoke(GetRandomOpeningLine());
        else instance.chatbot.Chat(request, instance.FinishedChat);
    }

    /// <summary>
    /// Callback function for the chatbot request.
    /// </summary>
    /// <param name="result">Chatbot result structure.</param>
    public void FinishedChat(ChatbotResult result) {
        results.Enqueue(result);
    }

    /// <summary>
    /// Handle the queued results of chatbot and invokes thier callbacks.
    /// </summary>
    private void Update() {
        if (results.Count > 0) {
            int itemsInQueue = results.Count;
            for (int i = 0; i < itemsInQueue; ++i) {
                ChatbotResult result = results.Dequeue();
                result.callback?.Invoke(result.response);
            }
        }
    }

    /// <summary>
    /// Terminate chatbot server process on application quit signal.
    /// </summary>
    private void OnApplicationQuit() {
        if (Available) chatbotProcess.CloseMainWindow();
    }
}

/// <summary>
/// Structure for containing result of chatbot request.
/// </summary>
public struct ChatbotResult {
    public string response;
    public Action<string> callback;

    public ChatbotResult(string response, Action<string> callback) {
        this.response = response;
        this.callback = callback;
    }
}

/// <summary>
/// Structure of a chatbot request.
/// </summary>
public struct ChatbotRequest {
    public string query;
    public Action<string> callback;

    public ChatbotRequest(string query, Action<string> callback) {
        this.query = query;
        this.callback = callback;
    }
}
