﻿using AsyncIO;
using NetMQ;
using NetMQ.Sockets;
using System.Collections;
using UnityEngine;
using System;
using System.Threading;

/// <summary>
/// <para>Class <c>Chatbot</c> handles sending chatbot requests to the chatbot server
/// and responses from the chatbot server.</para>
/// 
/// <para>
/// AUTHOR: Oleksandr Kononov
/// </para>
/// </summary>
public class Chatbot : MonoBehaviour {

    // Request socket for chatbot server.
    private RequestSocket client;

    /// <summary>
    /// Start function is the first after Awake functions.
    /// It is used to initialise the connection to the chatbot server.
    /// </summary>
    private void Start() {
        StartCoroutine(ConnectToChatbot());
    }

    /// <summary>
    /// Coroutine to connect to the chatbot server.
    /// </summary>
    /// <returns></returns>
    private IEnumerator ConnectToChatbot() {
        ForceDotNet.Force();
        client = new RequestSocket();
        client.Connect("tcp://localhost:5555");
        yield return null;
    }

    /// <summary>
    /// Send the chatbot request to the chatbot server.
    /// 
    /// <para>Note: creates a new thread when sending the request.</para>
    /// </summary>
    /// <param name="request">Chatbot request structure.</param>
    /// <param name="callback">Callback function for when the chatbot exchange has finished.</param>
    public void Chat(ChatbotRequest request, Action<ChatbotResult> callback) {
        new Thread(() => TryChat(request, callback)).Start();
    }

    /// <summary>
    /// Send the quit signal to the chatbot server to kill it.
    /// </summary>
    public void SendQuitSignal() {
        client.SendFrame("<QUIT_CHATBOT>");
    }

    /// <summary>
    /// Try to send the chatbot request to the chatbot server and invoke the callback upon completion.
    /// </summary>
    /// <param name="request">Chatbot request structure.</param>
    /// <param name="callback">Callback function to call when finished.</param>
    private void TryChat(ChatbotRequest request, Action<ChatbotResult> callback) {
        string response = "<NO_RESPONSE>";
        client.SendFrame(request.query);
        bool done = false;
        int attempts = 0;
        int maxAttempts = 5;
        while (!done) {
            if (attempts > maxAttempts) break;
            done = client.TryReceiveFrameString(new TimeSpan(0, 0, 5), out response);
            Thread.Sleep(100);
            attempts++;
        }
        response = response.Replace("unk", "***");
        callback(new ChatbotResult(response, request.callback));
    }

    /// <summary>
    /// Cleanup the connection on application quit request.
    /// </summary>
    private void OnApplicationQuit() {
        NetMQConfig.Cleanup(false);
    }
}
