﻿using FullSerializer;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// <para>Class <c>GeneticNeuralNetwork</c> is used to hold the structure of a neural network with
/// a genetic algorithm built-in.</para>
/// 
/// <para>
/// AUTHOR: Oleksandr Kononov
/// </para>
/// /// <para>
/// INSPIRED BY AUTHOR : The One
/// ,see <a href="https://www.youtube.com/watch?v=Yq0SfuiOVYE">link</a>.
/// </para>
/// </summary>
[System.Serializable]
public class GeneticNeuralNetwork : System.IComparable<GeneticNeuralNetwork> {

    // Constants for GeneticNeuralNetwork
    private const float BIAS = 0.35f;
    private const float WEIGHT_MUTATION_CHANCE = 0.8f;

    [fsProperty]
    private float fitness = 0; //Fitness value of this neural network
    [fsProperty]
    private int[] layers; // Layers of neurons
    [fsProperty]
    private float[][] neurons; // Neuron matrix [layerIndex][neuronIndex]
    [fsProperty]
    private float[][][] weights; // Weight matrix [layerIndex][neuronIndex][weightIndex]

    /// <summary>
    /// Add fitness to this genetic neural network.
    /// </summary>
    /// <param name="fit">Amount of fitness to add.</param>
    public void AddFitness(float fit) { fitness += fit; }

    /// <summary>
    /// Gets the fitness of this genetic neural network.
    /// </summary>
    public float Fitness { get { return fitness; } set { fitness = value; } }

    /// <summary>
    /// Constructor for the genetic nerual network class.
    /// </summary>
    /// <param name="layers">Amount of layer in the neural network.</param>
    public GeneticNeuralNetwork(int[] layers) {
        this.layers = new int[layers.Length];
        for (int i = 0; i < layers.Length; ++i) this.layers[i] = layers[i];
        InitNeurons();
        InitWeights();
    }

    /// <summary>
    /// Copy constructure for the genetic neural network class.
    /// </summary>
    /// <param name="copy">Genetic nerual network to copy from.</param>
    public GeneticNeuralNetwork(GeneticNeuralNetwork copy) {
        layers = new int[copy.layers.Length];
        for (int i = 0; i < copy.layers.Length; ++i) layers[i] = copy.layers[i];
        InitNeurons();
        InitWeights();
        CopyWeights(copy.weights);
    }

    /// <summary>
    /// Performs a deep copy of weights to current genetic neural network.
    /// </summary>
    /// <param name="copyWeights">3D matrix of weights to copy from.</param>
    private void CopyWeights(float[][][] copyWeights) {
        for (int i = 0; i < weights.Length; ++i) {
            for (int j = 0; j < weights[i].Length; ++j) {
                for (int k = 0; k < weights[i][j].Length; ++k) {
                    weights[i][j][k] = copyWeights[i][j][k];
                }
            }
        }
    }

    /// <summary>
    /// Initialise the neurons in the current genetic neural network.
    /// </summary>
    private void InitNeurons() {
        List<float[]> neuronsList = new List<float[]>();
        for (int i = 0; i < layers.Length; ++i) neuronsList.Add(new float[layers[i]]);
        neurons = neuronsList.ToArray();
    }

    /// <summary>
    /// Initialise the weights in the current genetic neural network.
    /// </summary>
    private void InitWeights() {
        List<float[][]> weightsList = new List<float[][]>();
        for (int i = 1; i < layers.Length; ++i) {
            List<float[]> layerWeightList = new List<float[]>();
            int neuronsInPreviousLayer = layers[i - 1];
            for (int j = 0; j < neurons[i].Length; ++j) {
                float[] neuronWeights = new float[neuronsInPreviousLayer];

                //Random weights -1 to 1
                for (int k = 0; k < neuronsInPreviousLayer; ++k) {
                    //Give random weight value to neuron weights
                    neuronWeights[k] = Random.Range(-0.5f, 0.5f);
                }
                layerWeightList.Add(neuronWeights);
            }
            weightsList.Add(layerWeightList.ToArray());
        }
        weights = weightsList.ToArray();
    }

    /// <summary>
    /// Perform a feed forward propogation of the genetic neural network.
    /// </summary>
    /// <param name="inputs">Inputs for the input layer of the network.</param>
    /// <returns></returns>
    public float[] FeedForward(float[] inputs) {
        for (int i = 0; i < inputs.Length; ++i) neurons[0][i] = inputs[i];

        for (int i = 1; i < layers.Length; ++i) {
            for (int j = 0; j < neurons[i].Length; ++j) {
                float value = BIAS;
                for (int k = 0; k < neurons[i - 1].Length; ++k) {
                    // Starting from the second layer, for each neuron get the weight * previous neuron sum value
                    value += weights[i - 1][j][k] * neurons[i - 1][k];
                }

                // Apply Activiation Function for the value (before setting it to current neuron
                neurons[i][j] = (float)System.Math.Tanh(value);

            }
        }

        //Return output layer neurons
        return neurons[neurons.Length - 1];
    }

    /// <summary>
    /// Performs the mutations of all weights of the genetic neural network.
    /// </summary>
    public void Mutate() {
        // Handle weight mutations
        for (int i = 0; i < weights.Length; ++i) {
            for (int j = 0; j < weights[i].Length; ++j) {
                for (int k = 0; k < weights[i][j].Length; ++k) {
                    weights[i][j][k] = MutateWeight(weights[i][j][k]);
                }
            }
        }
    }

    /// <summary>
    /// Mutate the weight of the current genetic neural network.
    /// </summary>
    /// <param name="weight">Initial weight</param>
    /// <returns></returns>
    private float MutateWeight(float weight) {
        float rnum = Random.Range(0f, 1000f); //Random number from 0 to 1000
        float hightestChance = 10 * WEIGHT_MUTATION_CHANCE;
        float chancePart = hightestChance / 4f; //Highest chance into four equal parts to determin exact mutation to apply

        if (rnum <= chancePart) weight *= -1f; //Flip sign mutation
        else if (rnum <= chancePart * 2) weight = UnityEngine.Random.Range(-0.5f, 0.5f); //Set new random value
        else if (rnum <= chancePart * 3) weight *= UnityEngine.Random.Range(0f, 1f) + 1f; //Increase by random factor of 0% to 100%
        else if (rnum <= hightestChance) weight *= UnityEngine.Random.Range(0f, 1f); //Decrease by random factor of 0% to 100%
        return weight;
    }

    /// <summary>
    /// Compares this genetic neural network to another one based on the fitness values.
    /// </summary>
    /// <param name="other"></param>
    /// <returns></returns>
    public int CompareTo(GeneticNeuralNetwork other) {
        if (other == null) return -1;
        if (fitness > other.fitness) return -1;
        else if (fitness < other.fitness) return 1;
        else return 0;
    }

}
