﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

/// <summary>
/// <para>Class <c>NeuralNetworkEnemyManager</c> contains all genetic neural networks and handles the 
/// spawning of enemies which will use these networks.</para>
/// 
/// <para>
/// AUTHOR: Oleksandr Kononov
/// </para>
/// </summary>
public class NeuralNetworkEnemyManager : MonoBehaviour {

    // Enemy to spawn
    [SerializeField]
    private GameObject enemyPrefab;
    [SerializeField]
    private int maxEnemyAmount = 6;
    [SerializeField]
    [Range(0, 5)]
    private float spawningRangeX, spawningRangeY;
    [SerializeField]
    private float spawningNewEnemiesTimer = 10f;
    private GameObject[] spawnLocations;

    // List of neural network agents and their genetic neural networks
    private List<NeuralNetworkAgent> enemyList = null;
    private volatile List<GeneticNeuralNetwork> neuralNetworks;

    /*
     * ***Input Layer***        |    ***Output Layer***
     * -------------------------------------------------
     * EnemyHealth              |       Attack
     * EnemyStamina             |       Defend
     * EnemyDamage              |       Run
     * EnemyDefence             |       Pursue
     * EnemyTargetDistance      |
     * TargetHealth             |
     * TargetDamage             |
     * TargetIsAttacking        |
     * TargetIsDefending        |
     * IsFacingTarget           |
     * 
     */
    private int[] initialLayers = new int[] { 10, 10, 10, (int)EnemyAttackAction.COUNT };

    /// <summary>
    /// Start function is the first after Awake functions.
    /// It is used to initialise the nerual networks and enemies.
    /// </summary>
    private void Start() {
        spawnLocations = GameObject.FindGameObjectsWithTag("WolfSpawn");
        if (spawnLocations == null || spawnLocations.Length == 0) {
            Debug.LogError("No WolfSpawn locations tags found for enemy spawning!");
            enabled = false;
        }
        if (enemyPrefab == null) {
            Debug.LogError("No EnemyPrefab!");
            enabled = false;
        }
        InitNeuralNetworks();
        SpawnAllEnemies();
    }

    /// <summary>
    /// Saves the currently fittest neural network to a persistent data path.
    /// </summary>
    private void SaveBestNeuralNetwork() {
        neuralNetworks.Sort();
        string json = Serializer.Serialize(typeof(GeneticNeuralNetwork), neuralNetworks[0]);
        Directory.CreateDirectory(Application.persistentDataPath + "/NeuralNetworks");
        File.WriteAllText(Application.persistentDataPath + "/NeuralNetworks/WolfNeuralNetwork.json", json);
    }

    /// <summary>
    /// Handle when the neural network agent is killed.
    /// </summary>
    /// <param name="enemy">Killed neural network agent.</param>
    /// <param name="network">Agents genetic neural network.</param>
    private void OnEnemyDeath(NeuralNetworkAgent enemy, GeneticNeuralNetwork network) {
        enemyList.Remove(enemy);
        if (this != null) StartCoroutine(SpawnReuseNetwork(network));
    }

    /// <summary>
    /// Reuse a previously defeated genetic neural network by assigning it the currently fitest weights 
    /// and spawn a new enemy with this network.
    /// </summary>
    /// <param name="network">Genetic neural network to reuse.</param>
    /// <returns></returns>
    private IEnumerator SpawnReuseNetwork(GeneticNeuralNetwork network) {
        yield return new WaitForSeconds(spawningNewEnemiesTimer);
        lock (neuralNetworks) {
            neuralNetworks.Sort();
        }
        network = new GeneticNeuralNetwork(neuralNetworks[0]);

        // Check if the genetic neural networks are allowed to continue training
        if (PlayerPrefs.GetInt("GeneticAlgorithm", 0) == 1) {
            network.Mutate();
            SaveBestNeuralNetwork();
        }
        SpawnEnemy(network);
    }

    /// <summary>
    /// Spawn a new enemy with a genetic neural network.
    /// </summary>
    /// <param name="network">Genetic neural network to use.</param>
    private void SpawnEnemy(GeneticNeuralNetwork network) {
        Vector2 spawnPos = spawnLocations[Random.Range(0, spawnLocations.Length)].transform.position;
        spawnPos += new Vector2(Random.Range(-spawningRangeX, spawningRangeX), Random.Range(-spawningRangeY, spawningRangeY));
        EnemyAttackDecisionModel e = Instantiate(enemyPrefab, spawnPos, Quaternion.identity).GetComponent<EnemyAttackDecisionModel>();
        e.Init(network);
        e.onDeathCallback += OnEnemyDeath;
        enemyList.Add(e);
    }

    /// <summary>
    /// Spawn all enemies at once (use for initial start of the game).
    /// </summary>
    private void SpawnAllEnemies() {
        if (enemyList == null) enemyList = new List<NeuralNetworkAgent>();
        for (int i=0; i < maxEnemyAmount; ++i) SpawnEnemy(neuralNetworks[i]);
    }

    /// <summary>
    /// Initialise all the genetic neural networks to be used.
    /// </summary>
    private void InitNeuralNetworks() {
        neuralNetworks = new List<GeneticNeuralNetwork>();

        // Load a template to use for all enemies initially
        GeneticNeuralNetwork neuralNetworkTemplate = LoadNeuralNetworkTemplate();

        for(int i=0; i < maxEnemyAmount; ++i) {
            GeneticNeuralNetwork network = new GeneticNeuralNetwork(neuralNetworkTemplate);
            if (PlayerPrefs.GetInt("GeneticAlgorithm", 0) == 1) network.Mutate();
            neuralNetworks.Add(network);
        }
    }

    /// <summary>
    /// Load a pre-trained genetic neural network.
    /// </summary>
    /// <returns></returns>
    private GeneticNeuralNetwork LoadNeuralNetworkTemplate() {
        // Check if there is a saved network in the persistent path first
        if (File.Exists(Application.persistentDataPath + "/NeuralNetworks/WolfNeuralNetwork.json")) {
            string jsonNet = File.ReadAllText(Application.persistentDataPath + "/NeuralNetworks/WolfNeuralNetwork.json");
            return (GeneticNeuralNetwork)Serializer.Deserialize(typeof(GeneticNeuralNetwork), jsonNet);
        }

        // Load default pre-trained network from resources
        TextAsset localNetwork = Resources.Load<TextAsset>("NeuralNetworks/WolfNeuralNetwork");
        if (localNetwork != null) {
            string jsonNet = localNetwork.text;
            File.WriteAllText(Application.persistentDataPath + "/NeuralNetworks/WolfNeuralNetwork.json", jsonNet);
            return (GeneticNeuralNetwork)Serializer.Deserialize(typeof(GeneticNeuralNetwork), jsonNet);
        }

        // If all else fails create a blank network with default layers
        return new GeneticNeuralNetwork(initialLayers);
    }

    /// <summary>
    /// Stop all coroutines on death.
    /// </summary>
    private void OnDestroy() {
        StopAllCoroutines();
    }
}
