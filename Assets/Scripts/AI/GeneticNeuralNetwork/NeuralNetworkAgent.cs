﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Output layer actions enums.
/// </summary>
public enum EnemyAttackAction { ATTACK, DEFEND, RUN, PURSUE, COUNT };

/// <summary>
/// <para>Abstract class <c>NeuralNetworkAgent</c> handles combat decision making for the agent using
/// their genetic neural network.</para>
/// 
/// <para>
/// AUTHOR: Oleksandr Kononov
/// </para>
/// </summary>
[RequireComponent(typeof(Character))]
public abstract class NeuralNetworkAgent : MonoBehaviour {

    protected GeneticNeuralNetwork network;
    protected Character character;

    // Agents states
    public bool Busy { get; set; }
    public bool Active { get; set; }
    public EnemyAttackAction LastAction { get { return lastAction; } }

    protected EnemyAttackAction lastAction;

    /// <summary>
    /// Enemy action behaviour callbacks.
    /// </summary>
    /// <param name="target">This enemy's target character.</param>
    public delegate void EnemyAction(Character target);

    /// <summary>
    /// OnDeath callback, used by manager for handling neural network of agent.
    /// </summary>
    /// <param name="agent">The neural network agent which died.</param>
    /// <param name="net">The genetic neural network on the agent which died.</param>
    public delegate void OnDeath(NeuralNetworkAgent agent, GeneticNeuralNetwork net);
    public OnDeath onDeathCallback;

    /// <summary>
    /// Awake runs once at the initialisation of the scene stage and
    /// before any other functions (with the exception of other Awake functions).
    /// 
    /// <para>Used for initialisation of the character variable.</para>
    /// </summary>
    protected void Awake() {
        character = GetComponent<Character>();
    }

    /// <summary>
    /// Initialises the genetic nerual network on this agent.
    /// 
    /// <para>Note: Must be called for the genetic neural network to work!</para>
    /// </summary>
    /// <param name="network">Reference to a genetic neural network that this agent will use.</param>
    public void Init(GeneticNeuralNetwork network) {
        this.network = network;
    }

    /// <summary>
    /// Function which will handle feeding inputs to the genetic neural network, extracting the output
    /// and invoking the corresponding action callbacks.
    /// </summary>
    /// <param name="target">Target character.</param>
    public abstract void MakeDecision(Character target);

    /// <summary>
    /// Calcualtes the fitness of the genetic nerual network for training.
    /// 
    /// <para>Note: Should be called after a decision action is performed to evaluate the action.</para>
    /// </summary>
    /// <param name="target">Target character.</param>
    public abstract void CalculateFitness(Character target);

    /// <summary>
    /// Determines from the genetic nerual network outputs which action to return.
    /// 
    /// <para>Note: Should be called at the end of <see cref="MakeDecision"/> function</para>
    /// </summary>
    /// <param name="outputs">Raw output of genetic neural network.</param>
    /// <returns></returns>
    protected abstract EnemyAttackAction GetEnemyAttackAction(float[] outputs);

    /// <summary>
    /// Softmax function to process genetic neural networks raw output.
    /// </summary>
    /// <param name="outputs">Raw output of genetic neural network.</param>
    /// <returns></returns>
    protected float[] Softmax(float[] outputs) {
        float sumOfE = 0;
        foreach (float f in outputs) sumOfE += Mathf.Exp(f);
        for (int i = 0; i < outputs.Length; ++i) outputs[i] = (Mathf.Exp(outputs[i]) / sumOfE);
        return outputs;
    }

    /// <summary>
    /// Normalises value with a given min and max vaules.
    /// </summary>
    /// <param name="value"></param>
    /// <param name="min"></param>
    /// <param name="max"></param>
    /// <returns></returns>
    protected float Normalize(float value, float min, float max) {
        return ((value - min) / (max - min));
    }

    /// <summary>
    /// Invoke on death callback when destroyed.
    /// </summary>
    public void OnDestroy() {
        if (onDeathCallback != null) onDeathCallback.Invoke(this, network);
    }
}
