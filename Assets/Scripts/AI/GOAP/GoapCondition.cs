﻿/// <summary>
/// Enum which contains all the GOAP conditions.
/// </summary>
[System.Serializable]
public enum GoapCondition {

    // Essential Goals
    HAS_FOOD,
    HAS_HEALTH,
    HAS_ENERGY,
    HAS_MONEY,
    IS_SOCIAL,
    ATTACKED,
    IDLE,

    // Villager Conditions
    TOOL,
    HAS_ORE,
    HAS_WOOD,

    COUNT
}