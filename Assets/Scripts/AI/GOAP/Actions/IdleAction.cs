﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// <para>Class <c>IdleAction</c> inherits from the <see cref="GoapAction"/> class.</para>
/// <para>It moves the agent to a random Location.</para>
/// 
/// <para>
/// AUTHOR: Oleksandr Kononov
/// </para>
/// </summary>
public class IdleAction : GoapAction {

    [SerializeField]
    private float idlingTimerMax = 3f;
    [SerializeField]
    private bool useRandomIdlingTimer = true;
    [SerializeField]
    private int wanderingRadius = 30;

    private bool reachedIdlePoint = false;
    private float idlingTimer = 0f;
    private float timer = 0f;

    /// <summary>
    /// Resets the Goap Action before use.
    /// </summary>
    public override void Reset() {
        reachedIdlePoint = false;
        GetComponent<PathfindingAgent>().StopPathfinding();
        timer = 0f;
    }

    /// <summary>
    /// Checks if the Goap Action is considered complete.
    /// </summary>
    /// <returns></returns>
    public override bool IsDone() {
        return reachedIdlePoint;
    }

    /// <summary>
    /// Checks if the Goap Action requires the Target/Location to be in range.
    /// </summary>
    /// <returns></returns>
    public override bool RequiresInRange() {
        return true;
    }

    /// <summary>
    /// Checks if this Goap Action is even allowed to run.
    /// Also sets the Target of the Goap Action.
    /// </summary>
    /// <param name="agent"></param>
    /// <returns></returns>
    public override bool CheckPrecondition(GameObject agent) {
        idlingTimer = useRandomIdlingTimer ? Random.Range(0, idlingTimerMax) : idlingTimerMax;
        Location = PathRequestManager.GetRandomWalkableDestination(agent, transform.position, wanderingRadius);
        return true;
    }

    /// <summary>
    /// Performs the Goap action.
    /// </summary>
    /// <param name="agent"></param>
    /// <returns></returns>
    public override bool Perform(GameObject agent) {
        timer += Time.deltaTime;
        if (timer < idlingTimer) return true;
        reachedIdlePoint = true;
        return true;
    }
}
