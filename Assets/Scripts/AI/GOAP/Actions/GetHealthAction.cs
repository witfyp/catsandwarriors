﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// <para>Class <c>GetHealthAction</c> inherits from the <see cref="GoapAction"/> class.</para>
/// <para>It performs the action of getting health and maintains
/// the condition <see cref="GoapCondition.HAS_HEALTH"/>.</para>
/// 
/// <para>
/// AUTHOR: Oleksandr Kononov
/// </para>
/// </summary>
[RequireComponent(typeof(Character))]
public class GetHealthAction : GoapAction {

    [SerializeField]
    private float stayInHealthHouseTimer = 2f;

    private Character character;
    private bool hasHealth = false;
    private bool inHouse = false;
    private float timer = 0f;

    /// <summary>
    /// Awake fetches the components on the agent.
    /// </summary>
    private void Awake() {
        character = GetComponent<Character>();
    }

    /// <summary>
    /// Constructor for the class.
    /// </summary>
    public GetHealthAction() {
        AddPrecondition(GoapCondition.HAS_HEALTH, false);
        AddEffect(GoapCondition.HAS_HEALTH, true);
    }

    /// <summary>
    /// Resets the Goap Action before use.
    /// </summary>
    public override void Reset() {
        hasHealth = false;
        inHouse = false;
        timer = 0f;
    }

    /// <summary>
    /// Checks if the Goap Action is considered complete.
    /// </summary>
    /// <returns></returns>
    public override bool IsDone() {
        if (hasHealth) Target = null;
        return hasHealth;
    }

    /// <summary>
    /// Checks if the Goap Action requires the Target/Location to be in range.
    /// </summary>
    /// <returns></returns>
    public override bool RequiresInRange() {
        return true;
    }

    /// <summary>
    /// Checks if this Goap Action is even allowed to run.
    /// Also sets the Target of the Goap Action.
    /// </summary>
    /// <param name="agent"></param>
    /// <returns></returns>
    public override bool CheckPrecondition(GameObject agent) {

        List<GameObject> healthLocations = new List<GameObject>(GameObject.FindGameObjectsWithTag("Health"));

        GameObject closestHealth = null;
        float closestDist = 0f;

        foreach (GameObject m in healthLocations) {
            if (closestHealth == null) {
                closestHealth = m;
                closestDist = (m.transform.position - agent.transform.position).magnitude;
                continue;
            }
            float d = (m.transform.position - agent.transform.position).magnitude;
            if (d < closestDist) {
                closestHealth = m;
                closestDist = d;
            }
        }

        Target = closestHealth;
        return Target != null;
    }

    /// <summary>
    /// Performs the Goap action.
    /// </summary>
    /// <param name="agent"></param>
    /// <returns></returns>
    public override bool Perform(GameObject agent) {
        InteractableHouse healthHouse = Target.GetComponent<InteractableHouse>();
        if (healthHouse == null) {
            hasHealth = true;
            character.Health.IncreaseValue(50);
            return true;
        }

        // Enter the food house
        if (!inHouse) {
            inHouse = healthHouse.OnInteract(character);
            return true;
        }

        timer += Time.deltaTime;
        if (timer < stayInHealthHouseTimer) return true;

        healthHouse.StopInteraction(character);
        hasHealth = true;
        character.Health.IncreaseValue(50);
        return true;
    }
}
