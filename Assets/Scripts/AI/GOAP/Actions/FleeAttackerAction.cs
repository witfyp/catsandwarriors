﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// <para>Class <c>FleeAttackerAction</c> inherits from the <see cref="GoapAction"/> class.</para>
/// <para>It moves the agent in the opposite direction to the attacker if attacked.</para>
/// 
/// <para>
/// AUTHOR: Oleksandr Kononov
/// </para>
/// </summary>
[RequireComponent(typeof(Character))]
public class FleeAttackerAction : GoapAction {

    [SerializeField]
    private float fleeDistance = 5f;
    private bool reachedSafety = false;

    /// <summary>
    /// Constructor for the class.
    /// </summary>
    public FleeAttackerAction() {
        AddPrecondition(GoapCondition.ATTACKED, true);
        AddEffect(GoapCondition.ATTACKED, false);
    }

    /// <summary>
    /// Resets the Goap Action before use.
    /// </summary>
    public override void Reset() {
        reachedSafety = false;
    }

    /// <summary>
    /// Checks if the Goap Action is considered complete.
    /// </summary>
    /// <returns></returns>
    public override bool IsDone() {
        return reachedSafety;
    }

    /// <summary>
    /// Checks if the Goap Action requires the Target/Location to be in range.
    /// </summary>
    /// <returns></returns>
    public override bool RequiresInRange() {
        return true;
    }

    /// <summary>
    /// Checks if this Goap Action is even allowed to run.
    /// Also sets the Target of the Goap Action.
    /// </summary>
    /// <param name="agent"></param>
    /// <returns></returns>
    public override bool CheckPrecondition(GameObject agent) {
        CharacterHealthStatModel health = agent.GetComponent<Character>().Health;
        if (health == null || health.attackedBy == null) return false;

        Vector2 dir = (transform.position - health.attackedBy.transform.position).normalized;
        Vector2 fleeLocation = dir * fleeDistance + new Vector2(Random.Range(-5f,5f), Random.Range(-5,5));
        float dist = fleeDistance;
        while (!PathRequestManager.IsWalkable(gameObject, fleeLocation) && dist > 1)
            fleeLocation = dir * (--dist) + new Vector2(Random.Range(-5f, 5f), Random.Range(-5, 5));
        
        Location = dist <= 1 ? PathRequestManager.GetRandomWalkableDestination(gameObject, transform.position, (int)fleeDistance) : fleeLocation;

        return true;
    }

    /// <summary>
    /// Performs the Goap action.
    /// </summary>
    /// <param name="agent"></param>
    /// <returns></returns>
    public override bool Perform(GameObject agent) {
        reachedSafety = true;
        agent.GetComponent<Character>().Health.attackedBy = null;
        return true;
    }
}
