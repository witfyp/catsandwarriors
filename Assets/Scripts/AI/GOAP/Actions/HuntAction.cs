﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// <para>Class <c>HuntAction</c> inherits from the <see cref="GoapAction"/> class.</para>
/// <para>It activates the Enemy Combat Neural Network on the Target(prey) and also maintains
/// the condition <see cref="GoapCondition.HAS_FOOD"/>.</para>
/// 
/// <para>
/// AUTHOR: Oleksandr Kononov
/// </para>
/// </summary>
[RequireComponent(typeof(EnemyAttackDecisionModel))]
public class HuntAction : GoapAction {

    [SerializeField]
    private float detectionRadius = 10f;

    private EnemyAttackDecisionModel decisionModel;

    private bool huntSuccessful = false;

    /// <summary>
    /// Awake fetches the components on the agent.
    /// </summary>
    private void Awake() {
        decisionModel = GetComponent<EnemyAttackDecisionModel>();
    }

    /// <summary>
    /// Constructor for the class.
    /// </summary>
    public HuntAction() {
        AddPrecondition(GoapCondition.HAS_FOOD, false);
        AddEffect(GoapCondition.HAS_FOOD, true);
        AddEffect(GoapCondition.HAS_HEALTH, true);
    }

    /// <summary>
    /// Resets the Goap Action before use.
    /// </summary>
    public override void Reset() {
        huntSuccessful = false;
    }

    /// <summary>
    /// Checks if the Goap Action is considered complete.
    /// </summary>
    /// <returns></returns>
    public override bool IsDone() {
        return huntSuccessful;
    }

    /// <summary>
    /// Checks if the Goap Action requires the Target/Location to be in range.
    /// </summary>
    /// <returns></returns>
    public override bool RequiresInRange() {
        return false; //The neural network will handle this
    }

    /// <summary>
    /// Checks if this Goap Action is even allowed to run.
    /// Also sets the Target of the Goap Action.
    /// </summary>
    /// <param name="agent"></param>
    /// <returns></returns>
    public override bool CheckPrecondition(GameObject agent) {
        List<GameObject> prey = new List<GameObject>();
        List<GameObject> ownKind = new List<GameObject>();

        foreach(Collider2D closeCollider in Physics2D.OverlapCircleAll(transform.position, detectionRadius)) {
            if (closeCollider.GetComponent<Character>() == null) continue;
            if (gameObject == closeCollider.gameObject) continue;
            if (closeCollider.tag == gameObject.tag) ownKind.Add(closeCollider.gameObject);
            else prey.Add(closeCollider.gameObject);
        }

        GameObject closestPrey = null;
        GameObject cloestOwnKind = null;

        foreach(GameObject p in prey) {
            if (closestPrey == null) {
                closestPrey = p;
                continue;
            }
            float distance = Vector2.Distance(transform.position, p.transform.position);
            float closestDistance = Vector2.Distance(transform.position, closestPrey.transform.position);
            if (distance < closestDistance) closestPrey = p;
        }

        foreach (GameObject c in ownKind) {
            if (cloestOwnKind == null) {
                cloestOwnKind = c;
                continue;
            }
            float distance = Vector2.Distance(transform.position, c.transform.position);
            float closestDistance = Vector2.Distance(transform.position, cloestOwnKind.transform.position);
            if (distance < closestDistance) cloestOwnKind = c;
        }

        Target = closestPrey;
        if (Target == null) Target = cloestOwnKind;
        return Target != null; 
    }

    /// <summary>
    /// Performs the Goap action.
    /// </summary>
    /// <param name="agent"></param>
    /// <returns></returns>
    public override bool Perform(GameObject agent) {
        if (Target == null || !PathRequestManager.IsInGrid(GetComponent<PathfindingAgent>().useGrid, Target)) {
            huntSuccessful = true;
            decisionModel.Active = false;
            agent.GetComponent<Character>().Hunger.IncreaseValue(50);
            agent.GetComponent<Character>().Health.IncreaseValue(10);
            return true;
        }
        decisionModel.Active = true;
        decisionModel.MakeDecision(Target.GetComponent<Character>());
        return true;
    }
}
