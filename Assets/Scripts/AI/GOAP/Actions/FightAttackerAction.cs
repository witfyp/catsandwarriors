﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// <para>Class <c>FightAttackerAction</c> inherits from the <see cref="GoapAction"/> class.</para>
/// <para>It switches to using the Enemy Combat Neural Network if attacked.</para>
/// 
/// <para>
/// AUTHOR: Oleksandr Kononov
/// </para>
/// </summary>
[RequireComponent(typeof(EnemyAttackDecisionModel))]
public class FightAttackerAction : GoapAction {

    // Reference to the agents Enemy Combat Neural Network
    private EnemyAttackDecisionModel decisionModel;

    private bool foughtBackAttacker = false;

    /// <summary>
    /// Awake fetches the components on the agent.
    /// </summary>
    private void Awake() {
        decisionModel = GetComponent<EnemyAttackDecisionModel>();
    }

    /// <summary>
    /// Constructor for the class.
    /// </summary>
    public FightAttackerAction() {
        AddPrecondition(GoapCondition.ATTACKED, true);
        AddEffect(GoapCondition.ATTACKED, false);
    }

    /// <summary>
    /// Resets the Goap Action before use.
    /// </summary>
    public override void Reset() {
        foughtBackAttacker = false;
    }

    /// <summary>
    /// Checks if the Goap Action is considered complete.
    /// </summary>
    /// <returns></returns>
    public override bool IsDone() {
        return foughtBackAttacker;
    }

    /// <summary>
    /// Checks if the Goap Action requires the Target/Location to be in range.
    /// </summary>
    /// <returns></returns>
    public override bool RequiresInRange() {
        return false; //The neural network will handle this
    }

    /// <summary>
    /// Checks if this Goap Action is even allowed to run.
    /// Also sets the Target of the Goap Action.
    /// </summary>
    /// <param name="agent"></param>
    /// <returns></returns>
    public override bool CheckPrecondition(GameObject agent) {
        CharacterHealthStatModel health = agent.GetComponent<Character>().Health;
        if (health == null | health.attackedBy == null) return false;
        Target = health.attackedBy.gameObject;
        return true;
    }

    /// <summary>
    /// Performs the Goap action.
    /// </summary>
    /// <param name="agent"></param>
    /// <returns></returns>
    public override bool Perform(GameObject agent) {
        if (Target == null || !PathRequestManager.IsInGrid(GetComponent<PathfindingAgent>().useGrid, Target)) {
            foughtBackAttacker = true;
            decisionModel.Active = false;
            agent.GetComponent<Character>().Health.attackedBy = null;
            agent.GetComponent<Character>().Hunger.IncreaseValue(50);
            return true;
        }
        // Invoke the genetic neural network to make a decision
        decisionModel.Active = true;
        decisionModel.MakeDecision(Target.GetComponent<Character>());
        return true;
    }
}
