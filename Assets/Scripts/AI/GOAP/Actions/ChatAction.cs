﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// <para>Class <c>ChatAction</c> inherits from the <see cref="GoapAction"/> class.</para>
/// <para>It performs the action of chatting by using the chatbot AI component and maintains
/// the condition <see cref="GoapCondition.IS_SOCIAL"/>.</para>
/// 
/// <para>
/// AUTHOR: Oleksandr Kononov
/// </para>
/// </summary>
public class ChatAction : GoapAction {

    // Private enum to handle different stages of the chat
    private enum ChatStage { INITIATE, DISPLAY, END };

    [SerializeField]
    private float happinessPerConversation = 2f;

    // Other villager
    private InteractableChatbot chatbot;
    private CharacterSocialStatModel otherSocial;

    // This villager
    private Character character;
    private SpeechBubble speechBubble;

    private bool preChatCheck = false;
    private bool thisCharacterTurn;
    private ChatStage stage;

    private string chatLine;

    /// <summary>
    /// Awake fetches the components on the agent.
    /// </summary>
    private void Awake() {
        character = GetComponent<Character>();
        speechBubble = GetComponent<InteractableChatbot>().speechBubble;
    }

    /// <summary>
    /// Constructor for the class.
    /// </summary>
    public ChatAction() {
        AddPrecondition(GoapCondition.IS_SOCIAL, false);
        AddEffect(GoapCondition.IS_SOCIAL, true);
    }

    /// <summary>
    /// Resets the Goap Action before use.
    /// </summary>
    public override void Reset() {
        thisCharacterTurn = true;
        stage = ChatStage.INITIATE;
        chatbot = null;
        otherSocial = null;
        preChatCheck = false;
        chatLine = ChatbotRequestManager.GetRandomOpeningLine();
    }

    /// <summary>
    /// Checks if the Goap Action is considered complete.
    /// </summary>
    /// <returns></returns>
    public override bool IsDone() {
        bool sociallySatisfied = character.Social.Value > (character.Social.MaxValue + character.Social.LowPoint)/2f;
        if (sociallySatisfied) chatbot.StopInteraction(character);
        return sociallySatisfied;
    }

    /// <summary>
    /// Checks if the Goap Action requires the Target/Location to be in range.
    /// </summary>
    /// <returns></returns>
    public override bool RequiresInRange() {
        return true;
    }

    /// <summary>
    /// Checks if this Goap Action is even allowed to run.
    /// Also sets the Target of the Goap Action.
    /// </summary>
    /// <param name="agent"></param>
    /// <returns></returns>
    public override bool CheckPrecondition(GameObject agent) {
        List<GameObject> villagers = new List<GameObject>();
        foreach(GameObject villager in GameObject.FindGameObjectsWithTag("Villager")) {
            if (villager.GetComponent<InteractableChatbot>() == null || villager == gameObject) continue;
            if (villager.GetComponent<CharacterMovementModel>().Frozen) continue;
            villagers.Add(villager);
        }

        GameObject closestVillager = null;
        float closestDist = 0f;

        foreach (GameObject v in villagers) {
            if (closestVillager == null) {
                closestVillager = v;
                closestDist = (v.transform.position - agent.transform.position).magnitude;
                continue;
            }
            float d = (v.transform.position - agent.transform.position).magnitude;
            if (d < closestDist) {
                closestVillager = v;
                closestDist = d;
            }
        }

        if (closestVillager == null) return false;

        Target = closestVillager;
        chatbot = Target.GetComponent<InteractableChatbot>();
        otherSocial = Target.GetComponent<CharacterSocialStatModel>();

        return Target != null;
    }

    /// <summary>
    /// Performs the Goap action.
    /// </summary>
    /// <param name="agent"></param>
    /// <returns></returns>
    public override bool Perform(GameObject agent) {
        if (!preChatCheck) if (Target.GetComponent<CharacterMovementModel>().Frozen) return false;
        preChatCheck = true;

        SpeechBubble currentSpeaker = thisCharacterTurn ? speechBubble : chatbot.speechBubble;
        
        // Go through the chat stages for a full conversation
        switch (stage) {
            case ChatStage.INITIATE:
                stage = ChatStage.DISPLAY;
                if (thisCharacterTurn) {
                    chatbot.OnInteract(character);
                    speechBubble.Show(chatLine);
                } else chatbot.ReceiveQuery(chatLine, ChatCallback);
                break;
            case ChatStage.DISPLAY:
                if (currentSpeaker.IsVisible()) stage = ChatStage.END;
                break;
            case ChatStage.END:
                if (!currentSpeaker.IsVisible()) {
                    stage = ChatStage.INITIATE;
                    thisCharacterTurn = !thisCharacterTurn;
                }
                break;
        }
        return true;
    }

    /// <summary>
    /// Chat callback for <see cref="ChatStage.INITIATE"/> stage.
    /// </summary>
    /// <param name="response"></param>
    private void ChatCallback(string response) {
        response = chatbot.ApplyPostChatRules(response);
        chatbot.speechBubble.Show(response);
        ChatbotRequestManager.Request(new ChatbotRequest(response, SetNewChatLineCallback));
    }

    /// <summary>
    /// Chat callback for the completion of the previous callback (response).
    /// </summary>
    /// <param name="newChatLine"></param>
    private void SetNewChatLineCallback(string newChatLine) {
        chatLine = newChatLine;
        character.Social.IncreaseValue(happinessPerConversation);
        otherSocial.IncreaseValue(happinessPerConversation);
    }
}
