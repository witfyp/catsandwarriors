﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// <para>Class <c>GetFoodAction</c> inherits from the <see cref="GoapAction"/> class.</para>
/// <para>It performs the action of getting food and maintains
/// the condition <see cref="GoapCondition.HAS_FOOD"/>.</para>
/// 
/// <para>
/// AUTHOR: Oleksandr Kononov
/// </para>
/// </summary>
[RequireComponent(typeof(Character))]
public class GetFoodAction : GoapAction {

    [SerializeField]
    private float stayInFoodHouseTimer = 2f;

    private Character character;
    private bool hasFood = false;
    private bool inHouse = false;
    private float timer = 0f;

    /// <summary>
    /// Awake fetches the components on the agent.
    /// </summary>
    private void Awake() {
        character = GetComponent<Character>();
    }

    /// <summary>
    /// Constructor for the class.
    /// </summary>
    public GetFoodAction() {
        AddPrecondition(GoapCondition.HAS_FOOD, false);
        AddEffect(GoapCondition.HAS_FOOD, true);
    }

    /// <summary>
    /// Resets the Goap Action before use.
    /// </summary>
    public override void Reset() {
        hasFood = false;
        inHouse = false;
        timer = 0f;
    }

    /// <summary>
    /// Checks if the Goap Action is considered complete.
    /// </summary>
    /// <returns></returns>
    public override bool IsDone() {
        if (hasFood) Target = null;
        return hasFood;
    }

    /// <summary>
    /// Checks if the Goap Action requires the Target/Location to be in range.
    /// </summary>
    /// <returns></returns>
    public override bool RequiresInRange() {
        return true;
    }

    /// <summary>
    /// Checks if this Goap Action is even allowed to run.
    /// Also sets the Target of the Goap Action.
    /// </summary>
    /// <param name="agent"></param>
    /// <returns></returns>
    public override bool CheckPrecondition(GameObject agent) {

        List<GameObject> foodLocations = new List<GameObject>(GameObject.FindGameObjectsWithTag("Food"));

        GameObject closestFood = null;
        float closestDist = 0f;

        foreach (GameObject m in foodLocations) {
            if (closestFood == null) {
                closestFood = m;
                closestDist = (m.transform.position - agent.transform.position).magnitude;
                continue;
            }
            float d = (m.transform.position - agent.transform.position).magnitude;
            if (d < closestDist) {
                closestFood = m;
                closestDist = d;
            }
        }

        Target = closestFood;
        return Target != null;
    }

    /// <summary>
    /// Performs the Goap action.
    /// </summary>
    /// <param name="agent"></param>
    /// <returns></returns>
    public override bool Perform(GameObject agent) {
        InteractableHouse foodHouse = Target.GetComponent<InteractableHouse>();
        if (foodHouse == null) {
            hasFood = true;
            character.Hunger.IncreaseValue(100);
            return true;
        }

        // Enter the food house
        if (!inHouse) {
            inHouse = foodHouse.OnInteract(character);
            return true;
        }

        timer += Time.deltaTime;
        if (timer < stayInFoodHouseTimer) return true;

        foodHouse.StopInteraction(character);
        hasFood = true;
        character.Hunger.IncreaseValue(100);
        return true;
    }
}
