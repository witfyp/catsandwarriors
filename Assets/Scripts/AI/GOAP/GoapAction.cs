﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// <para>Abstract class <c>GoapAction</c> contains all necessary abstract functions for a GOAP Action.
/// Any class that is to be accepted by GOAP as an action needs to have this as a base class.</para>
/// 
/// <para>
/// AUTHOR: Oleksandr Kononov
/// </para>
/// <para>
/// INSPIRED BY AUTHOR : Brent Owens 
/// ,see <a href="https://github.com/sploreg/goap">link</a>.
/// </para>
/// </summary>
public abstract class GoapAction: MonoBehaviour {

    // Costs that are associated with performing this action
    public float cost = 1.0f;
    public float energyCost = 0f;

    // Subject/Location this action is to be performed at
    public GameObject Target { get; set; }
    public Vector2 Location { get; set; }

    // Preconditions and Effects of performing this action
    private Dictionary<GoapCondition, object> preconditions;
    private Dictionary<GoapCondition, object> effects;

    private bool inRange = false;

    /// <summary>
    /// Constructor for the class.
    /// </summary>
    public GoapAction() {
        preconditions = new Dictionary<GoapCondition, object>();
        effects = new Dictionary<GoapCondition, object>();
    }

    /// <summary>
    /// Function which will reset any variables in the action class.
    /// 
    /// Is called once before performing the action.
    /// </summary>
    public abstract void Reset();

    /// <summary>
    /// Function which checks if the action is considered complete.
    /// 
    /// Is called every time after performing the action.
    /// </summary>
    /// <returns>Boolean if the action is considered complete.</returns>
    public abstract bool IsDone();

    /// <summary>
    /// Function which checks if this action is usable and can be considered for GOAP planning.
    /// 
    /// In this function the Target/Location variables should be assigned.
    /// </summary>
    /// <param name="agent">The GOAP agent who might be using this action.</param>
    /// <returns>Boolean if the preconditions are satified.</returns>
    public abstract bool CheckPrecondition(GameObject agent);

    /// <summary>
    /// Functions which will be called when the agent is in range to perform the action.
    /// 
    /// Will be called continously until the function <see cref="IsDone"/> returns <c>true</c>.
    /// </summary>
    /// <param name="agent">The GOAP agent who is using this action.</param>
    /// <returns>Boolean if the action was performed successfully.
    /// <para>Note: returning false will fail the current plan.</para></returns>
    public abstract bool Perform(GameObject agent);

    /// <summary>
    /// Function which checks if this action requires the Target/Location to be in range
    /// before performing the action.
    /// 
    /// </summary>
    /// <returns>Boolean if action requires to be in range.</returns>
    public abstract bool RequiresInRange();

    /// <summary>
    /// Performs a reset of base class action variables and calls the child action class
    /// <see cref="Reset"/> function.
    /// </summary>
    public void DoReset() {
        inRange = false;
        Target = null;
        Reset();
    }

    /// <summary>
    /// Function to add preconditions to this action.
    /// 
    /// For this action to be usable and considered by the GOAP planner, it needs to have the
    /// preconditions satisfied.
    /// </summary>
    /// <param name="key">A condition.</param>
    /// <param name="value">A value for the condition (usually a boolean is enough).</param>
    public void AddPrecondition(GoapCondition key, object value) {
        preconditions.Add(key, value);
    }

    /// <summary>
    /// Function that removes a precondition for the action.
    /// </summary>
    /// <param name="key">A condition to remove.</param>
    public void RemovePrecondition(GoapCondition key) {
        preconditions.Remove(key);
    }

    /// <summary>
    /// Function to add an effect to this action.
    /// 
    /// The effect shows how a given condition will change to when this action is performed.
    /// </summary>
    /// <param name="key">A condition that will be affected.</param>
    /// <param name="value">The value of the condition that will be affected.</param>
    public void AddEffect(GoapCondition key, object value) {
        effects.Add(key, value);
    }

    /// <summary>
    /// Function that removes an effect for the action.
    /// </summary>
    /// <param name="key">A condition to remove.</param>
    public void RemoveEffect(GoapCondition key, object value) {
        effects.Remove(key);
    }

    /// <summary>
    /// Getter and Setter for InRange variable which determines whether the agent is considered
    /// in range to perform an action.
    /// </summary>
    public bool InRange {
        get { return inRange; }
        set { inRange = value; }
    }

    /// <summary>
    /// Getter for all the preconditions of this action.
    /// </summary>
    public Dictionary<GoapCondition, object> Preconditions {
        get { return preconditions; }
    }

    /// <summary>
    /// Getter for all the effects of this action.
    /// </summary>
    public Dictionary<GoapCondition, object> Effects {
        get { return effects; }
    }
}
