﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// <para>Interface <c>IGOAP</c> acts as a data provider for the <see cref="GoapAgent"/>.</para>
/// <para>Any agent that wants to use GOAP must implement this interface.
/// It provides information to the GOAP planner so it can plan what actions to use.</para>
/// 
/// <para>
/// AUTHOR: Oleksandr Kononov
/// </para>
/// <para>
/// INSPIRED BY AUTHOR : Brent Owens 
/// ,see <a href="https://github.com/sploreg/goap">link</a>.
/// </para>
/// </summary>
public interface IGoap {

    /// <summary>
    /// Starting state of agent and the world.
    /// </summary>
    /// <returns>Dictionary of <see cref="GoapCondition"/> and object value.</returns>
    Dictionary<GoapCondition, object> GetWorldState();

    /// <summary>
    /// Give the planer a new goal
    /// </summary>
    /// <returns>Dictionary of <see cref="GoapCondition"/> and object value.</returns>
    Dictionary<GoapCondition, object> CreateGoalState();

    /// <summary>
    /// In the event the planning for this goal fails
    /// </summary>
    /// <param name="failedGoal">Goal which failed.</param>
    void PlanFailed(Dictionary<GoapCondition, object> failedGoal);

    /// <summary>
    /// A plan found for the goal supplied.
    /// </summary>
    /// <param name="goal">Goal for which the plan was found.</param>
    /// <param name="actions">Queue of Goap Actions to execute the plan.</param>
    void PlanFound(Dictionary<GoapCondition, object> goal, Queue<GoapAction> actions);

    /// <summary>
    /// When all actions where executed and goal was reached
    /// </summary>
    void ActionsFinished();

    /// <summary>
    /// When an action causes the plan to be aborted
    /// </summary>
    /// <param name="lastAction">Goap Action on which the plan failed.</param>
    void PlanAborted(GoapAction lastAction);

    /// <summary>
    /// Move the agent towards goal using A* Pathfinding algorithm.
    /// </summary>
    /// <param name="nextAction">Current Goap Action.</param>
    /// <returns>Returns true - if agent at target.
    /// Returns false - if agent has not reached the target yet.</returns>
    bool MoveAgent(GoapAction nextAction);
}
