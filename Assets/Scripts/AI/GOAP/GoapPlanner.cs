﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// <para>Class <c>GoapPlanner</c> hanles creating a plan of actions for the <see cref="GoapAgent"/> to execute.</para>
/// 
/// <para>
/// AUTHOR: Oleksandr Kononov
/// </para>
/// <para>
/// INSPIRED BY AUTHOR : Brent Owens 
/// ,see <a href="https://github.com/sploreg/goap">link</a>.
/// </para>
/// </summary>
public class GoapPlanner {

    /// <summary>
    /// Plan a sequence of actions that will fulfill that goal.
    /// </summary>
    /// <param name="agent">The agent which invoked the planner.</param>
    /// <param name="availableActions">Set of all Goap Actions on the agent.</param>
    /// <param name="worldState">Current state of the agent.</param>
    /// <param name="goal">Desired state of the agent.</param>
    /// <returns>Returns Queue<GoapAction> - if the queue of actions are found.
    /// Returns null - if no plan could be formed.</returns>
    public Queue<GoapAction> Plan(GameObject agent,
                                    HashSet<GoapAction> availableActions,
                                    Dictionary<GoapCondition, object> worldState,
                                    Dictionary<GoapCondition, object> goal) {
        foreach (GoapAction a in availableActions) a.DoReset();

        HashSet<GoapAction> usableActions = new HashSet<GoapAction>();
        foreach (GoapAction a in availableActions) {
            if (a.CheckPrecondition(agent)) usableActions.Add(a);
        }

        // Now here a tree structure is built - leaf nodes here provide solutions for the goal
        List<Node> leaves = new List<Node>();

        // Build tree
        Node root = new Node(null, 0, worldState, null);
        bool success = goal == null || goal.Count == 0 ? false : BuildTree(root, leaves, usableActions, goal);

        if (!success) {
            Debug.Log("GoapPlanner: NO PLAN");
            return null;
        }

        Node cheapest = null;
        foreach (Node leaf in leaves) {
            if (cheapest == null) cheapest = leaf;
            else if (leaf.runningCost < cheapest.runningCost) cheapest = leaf;
        }

        // Work backwards from the cheapest leaf node to get our plan
        List<GoapAction> result = new List<GoapAction>();
        Node n = cheapest;
        while (n != null) {
            if (n.action != null) result.Insert(0, n.action);
            n = n.parent;
        }

        Queue<GoapAction> queue = new Queue<GoapAction>();
        foreach (GoapAction a in result) queue.Enqueue(a);
        return queue;
    }

    /// <summary>
    /// Builds a tree of nodes and returns true if at least one solution was found.
    /// If it was, then all we have to do is follow the parent of the successful node 
    /// to find our actions that are needed to perform in order to reach the goal.
    /// </summary>
    /// <param name="parent">Parent node.</param>
    /// <param name="leaves">Leaf nodes.</param>
    /// <param name="usableActions">Set of usable Goap Actions.</param>
    /// <param name="goal">Desired state of the agent.</param>
    /// <returns></returns>
    private bool BuildTree(Node parent, List<Node> leaves, HashSet<GoapAction> usableActions, Dictionary<GoapCondition, object> goal) {
        bool foundOne = false;

        // Similar to Mini-Max algorithm where we apply a temporary state to see were it leads, then return everything back
        foreach(GoapAction action in usableActions) {
            
            // We can use this action only if the parent state has the conditions needed for this actions pre-conditions
            if (ConditionsMatchPreconditions(parent.state, action.Preconditions)) {

                // Apply temporary effects to parent state
                Dictionary<GoapCondition, object> currentState = ApplyEffectsToState(parent.state, action.Effects);
                Node node = new Node(parent, parent.runningCost + action.cost, currentState, action);

                // If we found a solution
                if (ConditionsMatchPreconditions(currentState, goal)) {
                    leaves.Add(node);
                    foundOne = true;
                } else {
                    // Didn't find a solution, so branch out and test remaining actions
                    HashSet<GoapAction> subset = ActionSubset(usableActions, action);
                    foundOne = BuildTree(node, leaves, subset, goal);
                }
            }
        }

        return foundOne;
    }

    /// <summary>
    /// Checking that the preconditions match the conditions for the action to be performed.
    /// </summary>
    /// <param name="conditions">Map of conditions and their values.</param>
    /// <param name="preconditions">Map of preconditions and their values.</param>
    /// <returns></returns>
    private bool ConditionsMatchPreconditions(Dictionary<GoapCondition, object> conditions, Dictionary<GoapCondition, object> preconditions) {
        foreach(GoapCondition precondition in preconditions.Keys) {
            if (!conditions.ContainsKey(precondition)) continue;
            if (!preconditions[precondition].Equals(conditions[precondition])) return false;
        }
        return true;
    }

    /// <summary>
    /// Apply the effect to a given state
    /// </summary>
    /// <param name="state">State to apply the effects to.</param>
    /// <param name="effects">Effects to apply.</param>
    /// <returns></returns>
    private Dictionary<GoapCondition, object> ApplyEffectsToState(Dictionary<GoapCondition, object> state, Dictionary<GoapCondition, object> effects) {
        Dictionary<GoapCondition, object> newState = new Dictionary<GoapCondition, object>(state);

        foreach(GoapCondition effect in effects.Keys) {
            if (!newState.ContainsKey(effect)) newState.Add(effect, effects[effect]);
            else newState[effect] = effects[effect];
        }

        return newState;
    }

    /// <summary>
    /// Create a subset of actions excluding the remove one. Creating a new set.
    /// </summary>
    /// <param name="actions">Set of usable Goap actions.</param>
    /// <param name="remove">Goap action to be removed.</param>
    /// <returns></returns>
    private HashSet<GoapAction> ActionSubset(HashSet<GoapAction> actions, GoapAction remove) {
        HashSet<GoapAction> subset = new HashSet<GoapAction>();
        foreach (GoapAction action in actions) {
            if (!action.Equals(remove)) subset.Add(action);
        }
        return subset;
    }

    /// <summary>
    /// Class <c>Node</c> is used as nodes for creating the tree of nodes which is done by the planner.
    /// </summary>
    private class Node {
        public Node parent;
        public float runningCost;
        public Dictionary<GoapCondition, object> state;
        public GoapAction action;

        /// <summary>
        /// Class constructor.
        /// </summary>
        /// <param name="parent">Parent node.</param>
        /// <param name="runningCost">Accumulated cost.</param>
        /// <param name="state">State at this node.</param>
        /// <param name="action">Goap action of this node.</param>
        public Node(Node parent, float runningCost, Dictionary<GoapCondition, object> state, GoapAction action) {
            this.parent = parent;
            this.runningCost = runningCost;
            this.state = state;
            this.action = action;
        }
    }
}
