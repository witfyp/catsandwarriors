﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// <para>Class <c>GoapAgent</c> handles invoking <see cref="GoapPlanner"/>, moving the agent,
/// and executing the Goap plan.</para>
/// 
/// <para>
/// AUTHOR: Oleksandr Kononov
/// </para>
/// <para>
/// INSPIRED BY AUTHOR : Brent Owens 
/// ,see <a href="https://github.com/sploreg/goap">link</a>.
/// </para>
/// </summary>
public class GoapAgent : MonoBehaviour {

    // Finite State Machine to handle the three GOAP states
    private FSM stateMachine;

    // GOAP states
    private FSM.FSMState idleState;
    private FSM.FSMState moveToState;
    private FSM.FSMState performActionState;

    // All actions available to the GOAP agent
    private HashSet<GoapAction> availableActions;
    private Queue<GoapAction> currentActions;

    // Data provider which implement the IGOAP interface
    private IGoap dataProvider;

    // Goap planner
    private GoapPlanner planner;

    /// <summary>
    /// Start function initialises all the FSM states and other variables.
    /// </summary>
    private void Start() {
        stateMachine = new FSM();
        availableActions = new HashSet<GoapAction>();
        currentActions = new Queue<GoapAction>();
        planner = new GoapPlanner();
        FindDataProvider();
        CreateIdleState();
        CreateMoveToState();
        CreatePerformActionState();
        stateMachine.PushState(idleState);
        LoadActions();
    }

    /// <summary>
    /// Updates the FSM for this GOAP agent.
    /// </summary>
    private void Update() {
        stateMachine.Update(this.gameObject);
    }

    /// <summary>
    /// Creates a new Goap plan for this agent.
    /// </summary>
    public void CreateNewPlan() {
        stateMachine.Clear();
        stateMachine.PushState(idleState);
    }

    /// <summary>
    /// Add a <see cref="GoapAction"/> to the agents <see cref="availableActions"/> set.
    /// </summary>
    /// <param name="a"></param>
    public void AddAction(GoapAction a) {
        availableActions.Add(a);
    }

    /// <summary>
    /// Getter for a Goap Action.
    /// </summary>
    /// <param name="action">Type of Goap Action to return.</param>
    /// <returns>Goap Action or null.</returns>
    public GoapAction GetAction(Type action) {
        foreach(GoapAction a in availableActions) {
            if (a.GetType().Equals(action)) return a;
        }
        return null;
    }

    /// <summary>
    /// Removes a given Goap Action from the availableActions set.
    /// </summary>
    /// <param name="action">Goap Action to remove.</param>
    public void RemoveAction(GoapAction action) {
        availableActions.Remove(action);
    }

    /// <summary>
    /// Check if the agent has an action plan.
    /// </summary>
    /// <returns></returns>
    private bool HasActionPlan() {
        return currentActions.Count > 0;
    }

    /// <summary>
    /// Creates and executes the FSM Idle state.
    /// </summary>
    private void CreateIdleState() {
        idleState = (fsm, gameObj) => {
            Dictionary<GoapCondition, object> worldState = dataProvider.GetWorldState();
            Dictionary<GoapCondition, object> goal = dataProvider.CreateGoalState();

            // Create a plan
            Queue<GoapAction> plan = planner.Plan(gameObject, availableActions, worldState, goal);
            if (plan != null) {
                currentActions = plan;
                dataProvider.PlanFound(goal, plan);

                fsm.PopState(); // Change FSM state to PerformAction state
                fsm.PushState(performActionState);
            } else {
                string g = "";
                foreach (GoapCondition c in goal.Keys) g = c.ToString();
                Debug.Log("<color=orange>Failed Plan! - Goal = "+g+"</color>", gameObject);
                dataProvider.PlanFailed(goal);
                fsm.PopState();
                fsm.PushState(idleState);
            }
        };
    }

    /// <summary>
    /// Creates and executes the FSM MoveTo state.
    /// </summary>
    private void CreateMoveToState() {
        moveToState = (fsm, gameObj) => {
            GoapAction action = currentActions.Peek();
            if (action.RequiresInRange() && (action.Target == null && action.Location == null)) {
                Debug.Log("<color=red>ERROR:</color> Action requires a targe but has none! Planning failed!");
                fsm.PopState(); // Pop MoveTo state
                fsm.PopState(); // Pop PerformAction state
                fsm.PushState(idleState);
                return;
            }

            if (dataProvider.MoveAgent(action)) {
                fsm.PopState();
            }
        };
    }

    /// <summary>
    /// Create and executes the FSM PerformAction state.
    /// </summary>
    private void CreatePerformActionState() {
        performActionState = (fsm, gameObj) => {

            if (!HasActionPlan()) {
                Debug.Log("<color=red>No Action Plan! Actions Done</color>");
                fsm.PopState();
                fsm.PushState(idleState);
                dataProvider.ActionsFinished();
                return;
            }

            GoapAction action = currentActions.Peek();

            if (action.IsDone()) currentActions.Dequeue();

            if (HasActionPlan()) {
                action = currentActions.Peek();
                bool inRange = action.RequiresInRange() ? action.InRange : true;

                if (inRange) {
                    bool success = action.Perform(gameObj);

                    if (!success) {
                        fsm.PopState();
                        fsm.PushState(idleState);
                        dataProvider.PlanAborted(action);
                    }
                } else {
                    fsm.PushState(moveToState);
                }
            } else {
                fsm.PopState();
                fsm.PushState(idleState);
                dataProvider.ActionsFinished();
            }
        };
    }

    /// <summary>
    /// Find and set the data provider variable, which is a class which inherits from the
    /// <see cref="IGoap"/> interface.
    /// </summary>
    private void FindDataProvider() {
        foreach (Component c in gameObject.GetComponents(typeof(Component))) {
            if (typeof(IGoap).IsAssignableFrom(c.GetType())) {
                dataProvider = (IGoap)c;
                return;
            }
        }
        Debug.Log("<color=red>ERROR:</color> DataProvider (IGOAP implementation) could not be found on GameObject - " + gameObject.name);
    }

    /// <summary>
    /// Load all the GOAP actions found on the agent.
    /// </summary>
    private void LoadActions() {
        GoapAction[] actions = gameObject.GetComponents<GoapAction>();
        foreach (GoapAction a in actions) availableActions.Add(a);
    }
}
