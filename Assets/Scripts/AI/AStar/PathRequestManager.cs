﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Threading;

/// <summary>
/// <para>Class <c>PathRequestManager</c> is a singleton class which handles all pathfinding requests.</para>
/// 
/// <para>
/// AUTHOR: Oleksandr Kononov
/// </para>
/// <para>
/// INSPIRED BY AUTHOR : Sebastian Lague 
/// ,see <a href="https://github.com/SebLague/Pathfinding">link</a>.
/// </para>
/// </summary>
[RequireComponent(typeof(Pathfinding))]
public class PathRequestManager : MonoBehaviour {

    // This classes instance
    public static PathRequestManager instance;

    // Access to the pathfinding class
    private Pathfinding pathfinding;

    // Queue of pathfinding results
    private Queue<PathResult> results = new Queue<PathResult>();

    /// <summary>
    /// Awake runs once at the initialisation of the scene stage and
    /// before any other functions (with the exception of other Awake functions).
    /// 
    /// <para>Used for initialisation singleton class and pathfinding.</para>
    /// </summary>
    private void Awake() {
        instance = this;
        pathfinding = GetComponent<Pathfinding>();
    }

    /// <summary>
    /// Check if a GameObject is in a grid of GridLocation
    /// </summary>
    /// <param name="grid">GridLocation</param>
    /// <param name="obj">GameObject</param>
    /// <returns>Boolean if GameObject is in the grid GridLocation</returns>
    public static bool IsInGrid(GridLocation grid, GameObject obj) {
        GridPath g = instance.pathfinding.GetGrid(grid);
        if (g == null) return false;
        return g.IsInGrid(obj);
    }

    /// <summary>
    /// Gets a random walkable location of the grid on which the GameObject is currently on.
    /// </summary>
    /// <param name="from">GameObject who is requesting the location.</param>
    /// <param name="origin">Vector2 world space origin for the location.</param>
    /// <param name="radius">Grid space radius of nodes for how large an area to search.</param>
    /// <returns>Vector2 world space position.</returns>
    public static Vector2 GetRandomWalkableDestination(GameObject from, Vector2 origin, int radius) {
        GridPath grid = instance.pathfinding.GetGrid(from);
        if (grid == null) return Vector2.zero;
        return grid.GetRandomWalkableDestination(origin, radius);
    }

    /// <summary>
    /// Checks if a given position is considered walkable by the grid of that GameObject.
    /// </summary>
    /// <param name="from">GameObject requesting this function.</param>
    /// <param name="position">Vector2 world space position to check.</param>
    /// <returns>Boolean if the position is walkable.</returns>
    public static bool IsWalkable(GameObject from, Vector2 position) {
        GridPath grid = instance.pathfinding.GetGrid(from);
        if (grid == null) return false;
        return grid.IsWalkable(position);
    }

    /// <summary>
    /// Locally update the grid of the GameObject with a given radius around the origin.
    /// </summary>
    /// <param name="from">GameObject requesting the update.</param>
    /// <param name="origin">Vector2 world space origin of the update.</param>
    /// <param name="radius">World space units of the radius of the update.</param>
    public static void UpdateGrid(GameObject from, Vector2 origin, float radius = 6f) {
        GridPath grid = instance.pathfinding.GetGrid(from);
        if (grid == null) return;
        instance.pathfinding.GetGrid(from).UpdateGrid(origin, radius);
    }

    /// <summary>
    /// Update all pathfinding grids.
    /// </summary>
    public static void UpdateGrids() {
        foreach (GridPath grid in instance.pathfinding.Grids) grid.UpdateGrid();
    }

    /// <summary>
    /// Update a specific pathfinding grid.
    /// </summary>
    /// <param name="location">Location of the grid to update.</param>
    public static void UpdateGrid(GridLocation location) {
        instance.pathfinding.GetGrid(location).UpdateGrid();
    }

    /// <summary>
    /// Locally update a pathfinding grid of the given location with a world space radius around the origin.
    /// </summary>
    /// <param name="location">Location of the grid to update.</param>
    /// <param name="origin">Vector2 origin of the update in world space.</param>
    /// <param name="radius">World space units of the radius of the update.</param>
    public static void UpdateGrid(GridLocation location, Vector2 origin, float radius = 6f) {
        instance.pathfinding.GetGrid(location).UpdateGrid(origin, radius);
    }

    /// <summary>
    /// Create a request for pathfinding.
    /// 
    /// <para>Note: creates a seperate thread to invoke the pathfinding.</para>
    /// </summary>
    /// <param name="request">Pathfinding request structure.</param>
    public static void RequestPath(PathRequest request) {
        ThreadStart threadStart = delegate {
            instance.pathfinding.FindPath(request, instance.FinishedProcessingPath);
        };
        threadStart.Invoke();
    }

    /// <summary>
    /// Callback function for pathfinding.
    /// </summary>
    /// <param name="result"></param>
    public void FinishedProcessingPath(PathResult result) {
        lock (results) {
            results.Enqueue(result);
        }
    }

    /// <summary>
    /// Handle the queued results of pathfinding and invokes thier callbacks.
    /// </summary>
    public void Update() {
        if (results.Count > 0) {
            int itemsInQueue = results.Count;
            lock (results) {
                for (int i = 0; i < itemsInQueue; ++i) {
                    PathResult result = results.Dequeue();
                    if (result.from != null) result.callback?.Invoke(result.path, result.success);
                }
            }
        }
    }
}

/// <summary>
/// Structure to contain the pathfinding result.
/// </summary>
public struct PathResult {
    public GameObject from;
    public Vector3[] path;
    public bool success;
    public Action<Vector3[], bool> callback;

    public PathResult(GameObject from, Vector3[] path, bool success, Action<Vector3[], bool> callback) {
        this.from = from;
        this.path = path;
        this.success = success;
        this.callback = callback;
    }
}

/// <summary>
/// Structure to contain the pathfinding requests.
/// </summary>
public struct PathRequest {
    public GameObject from;
    public Vector3 pathStart;
    public Vector3 pathEnd;
    public Action<Vector3[], bool> callback;

    public PathRequest(GameObject from, Vector3 start, Vector3 end, Action<Vector3[], bool> callback) {
        this.from = from;
        this.pathStart = start;
        this.pathEnd = end;
        this.callback = callback;
    }
}