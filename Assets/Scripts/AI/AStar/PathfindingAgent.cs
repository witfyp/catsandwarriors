﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// <para>Class <c>PathfindingAgent</c> is used by any agent who wishes to make use of the pathfinding
/// algorithm.</para>
/// 
/// <para>
/// AUTHOR: Oleksandr Kononov
/// </para>
/// <para>
/// INSPIRED BY AUTHOR : Sebastian Lague 
/// ,see <a href="https://github.com/SebLague/Pathfinding">link</a>.
/// </para>
/// </summary>
[RequireComponent(typeof(Character))]
[RequireComponent(typeof(CharacterMovementModel))]
public class PathfindingAgent : MonoBehaviour {

    // Constants
    private const float minPathUpdateTime = 0.2f;
    private const float pathUpdateMoveThreshold = 0.5f;

    // Global variables for use in the Editor
    public GridLocation useGrid;
    public bool drawGizmos;
    public float stoppingDistance = 0.35f;

    // The character of the pathfinding agent
    private Character character;

    // Destinations of the agent
    private Transform target;
    private Vector2 location;

    // Path calculated for the agent
    private Path path;

    // Pathfinding states
    private bool isRunning;
    private bool isDone;
    public bool Done { get { return isDone; } }
    public bool IsRunning { get { return isRunning; } }

    /// <summary>
    /// Start function is the first after Awake functions.
    /// It is used to initialise the states.
    /// </summary>
    private void Start() {
        isRunning = false;
        isDone = false;
        character = GetComponent<Character>();
    }

    /// <summary>
    /// Starting pathfinding and moving agent towards target.
    /// </summary>
    /// <param name="t">GameObject target</param>
    public void StartPathfinding(GameObject t) {
        if (t == null) return;
        StartPathfinding(t.transform);
    }

    /// <summary>
    /// Starting pathfinding and moving agent towards target.
    /// </summary>
    /// <param name="t">Transform target.</param>
    public void StartPathfinding(Transform t) {
        if (isRunning) return;
        isDone = false;
        target = t;
        StartCoroutine(UpdatePath());
    }

    /// <summary>
    /// Starting pathfinding and moving agent towards target.
    /// </summary>
    /// <param name="t">Vector2 world space target.</param>
    public void StartPathfinding(Vector2 t) {
        if (isRunning) return;
        isDone = false;
        location = t;
        StartCoroutine(UpdatePath());
    }

    /// <summary>
    /// Stop pathfinding and moving the agent, clear current target.
    /// </summary>
    public void StopPathfinding() {
        StopCoroutine("FollowPath");
        StopCoroutine(UpdatePath());
        target = null;
        character.Movement.SetDirection(Vector2.zero);
        isRunning = false;
    }

    /// <summary>
    /// Callback function for when the pathfinding algorithm has completed.
    /// </summary>
    /// <param name="waypoints">Vector3 primitive array of waypoints.</param>
    /// <param name="pathSuccess">Boolean on whether the pathfinding was successful.</param>
    public void OnPathFound(Vector3[] waypoints, bool pathSuccess) {
        if (pathSuccess) {
            path = new Path(waypoints, transform.position);
            StopCoroutine("FollowPath");
            StartCoroutine("FollowPath");
        }
    }

    /// <summary>
    /// Coroutine which will update the current path based on target positional changes or agents lack of movement.
    /// </summary>
    /// <returns></returns>
    IEnumerator UpdatePath() {
        // Allow everything to load
        if (Time.timeSinceLevelLoad < 0.3f) yield return new WaitForSeconds(0.3f);

        // Cannot proceed if there is no target
        while (target == null && location == null) yield return null;
        isRunning = true;

        // Make an initial request for a path
        if (target != null) PathRequestManager.RequestPath(new PathRequest(gameObject, transform.position, target.position, OnPathFound));
        else PathRequestManager.RequestPath(new PathRequest(gameObject, transform.position, location, OnPathFound));

        float sqrMoveThreshold = pathUpdateMoveThreshold * pathUpdateMoveThreshold;
        Vector2 targetPosOld = target != null ? new Vector2(target.position.x, target.position.y) : location;
        Vector3 agentPosOld = transform.position;

        // While agent is continuing to pathfind and move keep the path up to date
        while (!Done) {
            yield return new WaitForSeconds(minPathUpdateTime);

            // If the target has moved beyond the threshold, request a new path
            if (target != null) {
                bool targetMoved = (new Vector2(target.position.x, target.position.y) - targetPosOld).sqrMagnitude > sqrMoveThreshold;
                if (targetMoved) {
                    PathRequestManager.RequestPath(new PathRequest(gameObject, transform.position, target.position, OnPathFound));
                    targetPosOld = target.position;
                }
            } 
            // If the target is a location (which cannot move) and agent is not moving, request a new path
            else {
                bool agentMoved = Vector2.Distance(transform.position, agentPosOld) > 0;
                if (!agentMoved) {
                    location = PathRequestManager.GetRandomWalkableDestination(gameObject, transform.position, 5);
                    PathRequestManager.RequestPath(new PathRequest(gameObject, transform.position, location, OnPathFound));
                }
            }
        }
    }

    /// <summary>
    /// Coroutine which will move the agent along the current path.
    /// </summary>
    /// <returns></returns>
    IEnumerator FollowPath() {
        bool followingPath = true;
        int pathIndex = 0;
        if (path == null || path.lookPoints == null || path.lookPoints.Length == 0) yield return null;
        character.Movement.SetDirection((path.lookPoints[0] - transform.position).normalized);

        // Continue following the look points in the path until the agent reaches the end of path
        while (followingPath) {
            if (Vector2.Distance(path.lookPoints[pathIndex], transform.position) < stoppingDistance) pathIndex++;
            if (pathIndex >= path.lookPoints.Length) break;
            character.Movement.SetDirection((path.lookPoints[pathIndex] - transform.position).normalized);
            yield return null;
        }
        character.Movement.SetDirection(Vector2.zero);
        isDone = true;
    }

    /// <summary>
    /// Draws debug information (only visible in the Editor).
    /// </summary>
    public void OnDrawGizmos() {
        if (path == null || !drawGizmos) return;

        path.DrawWithGizmos();
    }

    /// <summary>
    /// Stops all pathfinding if the agent is destroyed.
    /// </summary>
    private void OnDestroy() {
        StopPathfinding();
    }
}
