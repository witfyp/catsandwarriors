﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Enum <c>GridLocation</c> is used to specify the pathfinding grids location.
/// 
/// This is used by pathfinding agents and utility grid functions.
/// 
/// <para>AUTHOR: Oleksandr Kononov</para>
/// </summary>
public enum GridLocation { VILLAGE, FOREST };

/// <summary>
/// <para>Class <c>GridPath</c> creates a logical grid of nodes which the
/// <see cref="Pathfinding"/> class will use to create a path.</para>
/// 
/// <para>Additionally the <c>GridPath</c> class provides utility functions
/// that are related to the grid.</para>
/// 
/// <example>
/// <code>public Vector2 GetRandomWalkableDestination(Vector2 origin, int radius) ...</code>
/// </example>
/// 
/// <para>
/// AUTHOR: Oleksandr Kononov
/// </para>
/// <para>
/// INSPIRED BY AUTHOR : Sebastian Lague 
/// ,see <a href="https://github.com/SebLague/Pathfinding">link</a>.
/// </para>
/// </summary>
public class GridPath : MonoBehaviour {

    /// <summary>
    /// Class <c>TerrainType</c> is used to couple terrain layermask and it's cost.
    /// </summary>
    [System.Serializable]
    public class TerrainType {
        public LayerMask terrainMask;
        public int terrainCost;
    }

    // This grids location
    public GridLocation gridLocation;

    // Global public variables for making adjustments in the Editor
    public LayerMask unwalkableMask;
    public Vector2 gridWorldSize;
    public float nodeRadius;
    public bool displayGridGizmos;
    public TerrainType[] walkableRegions;
    public int obstaclePromximityCost = 50;

    // Logical grid of nodes
    private Node[,] grid;

    // Dictionary of region layers -> region costs
    private Dictionary<int, int> walkableRegionsDictionary;
    private LayerMask walkableMask;

    private float nodeDiameter;
    private int gridSizeX, gridSizeY;

    private int costMin = int.MaxValue;
    private int costMax = int.MinValue;

    /// <summary>
    /// Returns the maximum area of the grid.
    /// </summary>
    public int MaxSize { get { return gridSizeX * gridSizeY; } }

    /// <summary>
    /// Awake runs once at the initialisation of the scene stage and
    /// before any other functions (with the exception of other Awake functions).
    /// 
    /// <para>Used for initialisation of variables and initiating grid creation.</para>
    /// </summary>
    private void Awake() {
        walkableRegionsDictionary = new Dictionary<int, int>();
        walkableMask = new LayerMask();

        nodeDiameter = nodeRadius * 2;
        gridSizeX = Mathf.RoundToInt(gridWorldSize.x / nodeDiameter);
        gridSizeY = Mathf.RoundToInt(gridWorldSize.y / nodeDiameter);

        foreach (TerrainType region in walkableRegions) {
            walkableMask.value |= region.terrainMask.value;
            walkableRegionsDictionary.Add((int)Mathf.Log(region.terrainMask.value, 2), region.terrainCost);
        }

        CreateGrid();
    }

    /// <summary>
    /// <para>Creates a logical grid of nodes of <c>gridSizeX</c> by <c>gridSizeY</c> of size.</para>
    /// 
    /// <para>Takes into account walkable/unwalkable regions and assignments region cost to nodes.</para>
    /// </summary>
    private void CreateGrid() {

        grid = new Node[gridSizeX, gridSizeY];
        Vector2 worldBottomLeft = transform.position - Vector3.right * gridWorldSize.x / 2 - Vector3.up * gridWorldSize.y / 2;

        // Loop through the x and y coordinates and create the node for its position in the grid
        for (int x = 0; x < gridSizeX; ++x) {
            for (int y = 0; y < gridSizeY; ++y) {
                Vector2 worldPoint = worldBottomLeft + 
                    Vector2.right * (x * nodeDiameter + nodeRadius) + Vector2.up * (y * nodeDiameter + nodeRadius);
                bool walkable = !Physics2D.OverlapCircle(worldPoint, nodeRadius, unwalkableMask);
                int movementCost = 0;

                // Cast a ray downward to see which region the node belongs to, so that we can assign appropriate cost
                Ray ray = new Ray(new Vector3(worldPoint.x, worldPoint.y, 0) + Vector3.forward * 50, Vector3.down);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit, 100, walkableMask)) {
                    walkableRegionsDictionary.TryGetValue(hit.collider.gameObject.layer, out movementCost);
                }

                if (!walkable) movementCost += obstaclePromximityCost;

                grid[x, y] = new Node(walkable, worldPoint, x, y, movementCost);
            }
        }

        // Blur the costs for the nodes in the grid
        BlurCostMap(2);
    }

    /// <summary>
    /// Blurs the movement cost of nodes in the grid map to reduce rigid percision
    /// of paths calculated through pathfinding.
    /// 
    /// <para>The blurring algorithm used is the Gaussian blur
    /// ,see <a href="https://en.wikipedia.org/wiki/Gaussian_blur#Implementation">link</a>.</para>
    /// </summary>
    /// <param name="blurSize">Controls the blur amount.</param>
    private void BlurCostMap(int blurSize) {
        int kernelSize = blurSize * 2 + 1;
        int kernelExtents = (kernelSize - 1) / 2;

        int[,] costHorizontalPass = new int[gridSizeX, gridSizeY];
        int[,] costVeticalPass = new int[gridSizeX, gridSizeY];

        // Loop horizontally over the grid nodes and apply the first blur to the nodes cost
        for (int y = 0; y < gridSizeY; ++y) {
            for (int x = -kernelExtents; x <= kernelExtents; ++x) {
                int sampleX = Mathf.Clamp(x, 0, kernelExtents);
                costHorizontalPass[0, y] += grid[sampleX, y].cost;
            }
            for (int x = 1; x < gridSizeX; ++x) {
                int removeIndex = Mathf.Clamp(x - kernelExtents - 1, 0, gridSizeX);
                int addIndex = Mathf.Clamp(x + kernelExtents, 0, gridSizeX - 1);
                costHorizontalPass[x, y] = costHorizontalPass[x - 1, y] - grid[removeIndex, y].cost + grid[addIndex, y].cost;
            }
        }

        // Loop vertically over the grid nodes and using the previous horizontal pass matrix complete the blurring algorithm
        for (int x = 0; x < gridSizeX; ++x) {
            for (int y = -kernelExtents; y <= kernelExtents; ++y) {
                int sampleY = Mathf.Clamp(y, 0, kernelExtents);
                costVeticalPass[x, 0] += costHorizontalPass[x, sampleY];
            }

            int blurredCost = Mathf.RoundToInt((float)costVeticalPass[x, 0] / (kernelSize * kernelSize));
            grid[x, 0].cost = blurredCost;

            for (int y = 1; y < gridSizeY; ++y) {
                int removeIndex = Mathf.Clamp(y - kernelExtents - 1, 0, gridSizeY);
                int addIndex = Mathf.Clamp(y + kernelExtents, 0, gridSizeY - 1);
                costVeticalPass[x, y] = costVeticalPass[x, y - 1] - costHorizontalPass[x, removeIndex] + costHorizontalPass[x, addIndex];
                blurredCost = Mathf.RoundToInt((float)costVeticalPass[x, y] / (kernelSize * kernelSize));
                grid[x, y].cost = blurredCost;
                if (blurredCost > costMax) costMax = blurredCost;
                else if (blurredCost < costMin) costMin = blurredCost;
            }
        }
    }

    /// <summary>
    /// Locally updates the cost values of the nodes of the grid with a given radius.
    /// </summary>
    /// <param name="origin">World coordinate origin point for the grid update</param>
    /// <param name="radius">World units radius for how much of the grid to update</param>
    public void UpdateGrid(Vector2 origin, float radius) {
        Node originNode = NodeFromWorldPoint(origin);
        int nradius = (int)(radius / nodeRadius);

        // Perform safe checks to obtain looping start and end points
        int xMin = originNode.gridX - nradius < 0 ? 0 : originNode.gridX - nradius;
        int xMax = originNode.gridX + nradius >= gridSizeX ? gridSizeX : originNode.gridX + nradius;
        int yMin = originNode.gridY - nradius < 0 ? 0 : originNode.gridY - nradius;
        int yMax = originNode.gridY + nradius >= gridSizeY ? gridSizeY : originNode.gridY + nradius;

        for (int x = xMin; x < xMax; ++x) {
            for(int y = yMin; y < yMax; ++y) {
                UpdateNode(x, y);
            }
        }
        BlurCostMap(2);
    }

    /// <summary>
    /// Globally updates the cost of nodes of the whole grid.
    /// </summary>
    public void UpdateGrid() {
        for (int x = 0; x < gridSizeX; ++x) {
            for (int y = 0; y < gridSizeY; ++y) {
                UpdateNode(x, y);
            }
        }
        BlurCostMap(2);
    }

    /// <summary>
    /// Updates the cost of a single node given it's <c>x</c> and <c>y</c>
    /// values in the logial grids matrix.
    /// </summary>
    /// <param name="x">Node x corrdinate in the logical grid</param>
    /// <param name="y">Node y corrdinate in the logical grid</param>
    private void UpdateNode(int x, int y) {
        bool walkable = !Physics2D.OverlapCircle(grid[x, y].worldPosition, nodeRadius, unwalkableMask);
        int movementCost = 0;

        Ray ray = new Ray(new Vector3(grid[x, y].worldPosition.x, grid[x, y].worldPosition.y, 0) + Vector3.forward * 50, Vector3.down);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 100, walkableMask)) {
            walkableRegionsDictionary.TryGetValue(hit.collider.gameObject.layer, out movementCost);
        }

        if (!walkable) movementCost += obstaclePromximityCost;

        grid[x, y].walkable = walkable;
        grid[x, y].cost = movementCost;
    }

    /// <summary>
    /// Find node neighbours in the grid.
    /// </summary>
    /// <param name="node">The node for which the neighbours found.</param>
    /// <returns>List of nodes that are the neighbours to the node passed in as a parameter.</returns>
    public List<Node> GetNeighbours(Node node) {
        List<Node> neighbours = new List<Node>();

        for (int x = -1; x <= 1; ++x) {
            for (int y = -1; y <= 1; ++y) {
                if (x == 0 && y == 0) continue;
                int checkX = node.gridX + x;
                int checkY = node.gridY + y;

                if (checkX >= 0 && checkX < gridSizeX && checkY >= 0 && checkY < gridSizeY)
                    neighbours.Add(grid[checkX, checkY]);
            }
        }
        return neighbours;
    }

    /// <summary>
    /// Checks if the given position is considered walkable by the grid.
    /// </summary>
    /// <param name="position">World coordinate position.</param>
    /// <returns>Boolean if the position is walkable.</returns>
    public bool IsWalkable(Vector2 position) {
        return NodeFromWorldPoint(position).walkable;
    }

    /// <summary>
    /// Check if the given node and all it's neighbours are walkable.
    /// </summary>
    /// <param name="n">The center node.</param>
    /// <returns>Boolean if the node and it's neightbours are walkable.</returns>
    public bool IsWalkableWithNeightbours(Node n) {
        foreach(Node node in GetNeighbours(n)) if (!node.walkable) return false;
        return n.walkable;
    }

    /// <summary>
    /// Searches for a walkable position in the grid given an origin and radius.
    /// </summary>
    /// <param name="origin">World coordinate origin point for the grid.</param>
    /// <param name="radius">Grid units radius for how much nodes to search.</param>
    /// <returns>Vector2 world position of a walkable position.</returns>
    public Vector2 GetRandomWalkableDestination(Vector2 origin, int radius) {
        Node originNode = NodeFromWorldPoint(origin);

        // Determine the min and max values for which to search
        int xMin = originNode.gridX - radius < 0 ? 0 : originNode.gridX - radius;
        int xMax = originNode.gridX + radius >= gridSizeX ? gridSizeX - 1 : originNode.gridX + radius;
        int yMin = originNode.gridY - radius < 0 ? 0 : originNode.gridY - radius;
        int yMax = originNode.gridY + radius >= gridSizeY ? gridSizeY - 1 : originNode.gridY + radius;

        Node node = grid[Random.Range(xMin, xMax), Random.Range(yMin, yMax)];

        while (true) {
            if (IsWalkableWithNeightbours(node) && node != originNode) break;
            node = grid[Random.Range(xMin, xMax), Random.Range(yMin, yMax)];
        }

        return node.worldPosition;
    }

    /// <summary>
    /// Converts world position to grid node.
    /// </summary>
    /// <param name="worldPosition">Vector3 world position.</param>
    /// <returns>Node from the grid.</returns>
    public Node NodeFromWorldPoint(Vector3 worldPosition) {
        Vector2 roundedPos = new Vector2(Mathf.RoundToInt(worldPosition.x), Mathf.RoundToInt(worldPosition.y));
        float minDist = float.PositiveInfinity;
        Node closestNode = null;

        // Loops through the grid to find the node which closely matches the world position
        for(int i = 0; i < gridSizeX; ++i) {
            for (int j = 0; j < gridSizeY; ++j) {
                float sqrDist = (grid[i, j].worldPosition - roundedPos).sqrMagnitude;
                if (sqrDist < minDist) {
                    minDist = sqrDist;
                    closestNode = grid[i, j];
                }
            }
        }

        return closestNode;
    }

    /// <summary>
    /// Checks if a GameObject is in a grid.
    /// </summary>
    /// <param name="obj">GameObject to check.</param>
    /// <returns>Boolean if the GameObject is in the current grid.</returns>
    public bool IsInGrid(GameObject obj) {
        if (obj.GetComponent<PathfindingAgent>() != null) return obj.GetComponent<PathfindingAgent>().useGrid == gridLocation;
        Node objNode = NodeFromWorldPoint(obj.transform.position);
        return Vector2.Distance(objNode.worldPosition, obj.transform.position) < nodeDiameter;
    }

    /// <summary>
    /// Draws debug information (only visible in the Editor).
    /// </summary>
    void OnDrawGizmos() {

        // Draw grid range.
        Gizmos.DrawWireCube(transform.position, new Vector3(gridWorldSize.x, gridWorldSize.y, 1));

        // Draw whole grid.
        if (grid != null && displayGridGizmos) {
            foreach (Node n in grid) {
                Gizmos.color = Color.Lerp(Color.white, Color.black, Mathf.InverseLerp(costMin, costMax, n.cost));

                Gizmos.color = n.walkable ? Gizmos.color : Color.red;
                Gizmos.color = new Color(Gizmos.color.r, Gizmos.color.g, Gizmos.color.b, 0.5f);
                Gizmos.DrawWireCube(n.worldPosition, Vector3.one * nodeDiameter);
            }
        }
    }
}
