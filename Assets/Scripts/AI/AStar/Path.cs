﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// <para>Class <c>Path</c> is the path calculated by the pathfinding algorithm and contains
/// the walking points to the target.</para>
/// 
/// <para>
/// AUTHOR: Oleksandr Kononov
/// </para>
/// <para>
/// INSPIRED BY AUTHOR : Sebastian Lague 
/// ,see <a href="https://github.com/SebLague/Pathfinding">link</a>.
/// </para>
/// </summary>
public class Path {
    // Walking points in world space
    public readonly Vector3[] lookPoints;

    private Color gizmoColour;
    private Vector3 gizmoStartPos;

    /// <summary>
    /// Constructor for the Path class.
    /// </summary>
    /// <param name="waypoints">Array of walking points in world space.</param>
    /// <param name="startPos">Starting position in world space.</param>
    public Path(Vector3[] waypoints, Vector3 startPos) {
        lookPoints = waypoints;
        gizmoColour = Random.ColorHSV();
        gizmoStartPos = startPos;
    }

    /// <summary>
    /// Gizmo method that will draw the path in the Editor
    /// </summary>
    public void DrawWithGizmos() {
        Gizmos.color = gizmoColour;
        Vector3 prevPoint = gizmoStartPos;
        foreach (Vector3 p in lookPoints) {
            Gizmos.DrawLine(prevPoint - Vector3.forward, p - Vector3.forward);
            prevPoint = p;
        }
    }
}
