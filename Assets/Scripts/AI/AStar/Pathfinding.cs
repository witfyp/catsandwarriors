﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Diagnostics;
using System;

/// <summary>
/// <para>Class <c>Pathfinding</c> is used to perform the pathfinding algorithm
/// and calcualte a path.</para>
/// 
/// <para>
/// AUTHOR: Oleksandr Kononov
/// </para>
/// <para>
/// INSPIRED BY AUTHOR : Sebastian Lague 
/// ,see <a href="https://github.com/SebLague/Pathfinding">link</a>.
/// </para>
/// </summary>
public class Pathfinding : MonoBehaviour {

    // A list of all grids in the game
    private List<GridPath> grids;

    // Debugging information for Gizmo drawining
    [SerializeField]
    private bool drawGizmoEnd = false;
    private Vector2 endLocation;

    /// <summary>
    /// Start function is the first after Awake functions.
    /// It is used to initialise the grids list.
    /// </summary>
    private void Start() {
        grids = new List<GridPath>(FindObjectsOfType<GridPath>());
        if (grids == null || grids.Count == 0) {
            UnityEngine.Debug.LogError("Pathfinding Cannot Find Any Grids");
            enabled = false;
        }
    }

    /// <summary>
    /// Get the list of grids.
    /// </summary>
    public List<GridPath> Grids { get { return grids; } }

    /// <summary>
    /// Get the grid which the GameObject is currently in.
    /// </summary>
    /// <param name="of">GameObject for which to find the grid.</param>
    /// <returns>Grid of the GameObject.</returns>
    public GridPath GetGrid(GameObject of) {
        foreach(GridPath grid in grids) if (grid.IsInGrid(of)) return grid;
        return null;
    }

    /// <summary>
    /// Get the grid of a given GridLocation
    /// </summary>
    /// <param name="location">GridLocation</param>
    /// <returns>Grid of the GridLocation</returns>
    public GridPath GetGrid(GridLocation location) {
        foreach(GridPath grid in grids) if (grid.gridLocation == location) return grid;
        return null;
    }

    /// <summary>
    /// Perform the A* pathfinding algorithm to find a path.
    /// </summary>
    /// <param name="request">Structure containing the request.</param>
    /// <param name="callback">Callback function to call once the pathfinding has finished.</param>
    public void FindPath(PathRequest request, Action<PathResult> callback) {
        Vector3[] waypoints = new Vector3[0];
        bool pathSuccess = false;

        // Get the grid to use for pathfinding
        GridPath grid = GetGrid(request.from.GetComponent<PathfindingAgent>().useGrid);
        if (grid == null) {
            callback(new PathResult(request.from, waypoints, pathSuccess, request.callback));
            return;
        }

        Node startNode = grid.NodeFromWorldPoint(request.pathStart);
        Node targetNode = grid.NodeFromWorldPoint(request.pathEnd);
        endLocation = request.pathEnd;

        if (startNode.walkable && targetNode.walkable) {
            Heap<Node> openSet = new Heap<Node>(grid.MaxSize);
            HashSet<Node> closedSet = new HashSet<Node>();

            openSet.Add(startNode);

            while (openSet.Count > 0) {

                // Get currentNode with lowest fCost from Heap
                Node currentNode = openSet.RemoveFirst();
                closedSet.Add(currentNode);

                if (currentNode == targetNode) {
                    pathSuccess = true;
                    break;
                }

                // For each current node neighbour calculate it's cost and add it to openSet (if it's not in the closedSet)
                foreach (Node neighbour in grid.GetNeighbours(currentNode)) {
                    if (!neighbour.walkable || closedSet.Contains(neighbour)) continue;

                    int newMovementCostToNeighbour = currentNode.gCost + GetDistance(currentNode, neighbour) + neighbour.cost;
                    if (newMovementCostToNeighbour < neighbour.gCost || !openSet.Contains(neighbour)) {
                        neighbour.gCost = newMovementCostToNeighbour;
                        neighbour.hCost = GetDistance(neighbour, targetNode);
                        neighbour.parent = currentNode;

                        if (!openSet.Contains(neighbour)) openSet.Add(neighbour);
                        else openSet.UpdateItem(neighbour);
                    }
                }
            }
        }

        if (pathSuccess) {
            waypoints = RetracePath(startNode, targetNode);
            pathSuccess = waypoints.Length > 0;
        }
        callback(new PathResult(request.from, waypoints, pathSuccess, request.callback));
    }

    /// <summary>
    /// Converts the produced Node Tree into an array of walkable world space vectors.
    /// </summary>
    /// <param name="startNode">Starting node.</param>
    /// <param name="targetNode">Target node.</param>
    /// <returns>Primitive array of Vector3 coordinates in world space.</returns>
    private Vector3[] RetracePath(Node startNode, Node targetNode) {
        List<Node> path = new List<Node>();
        Node currentNode = targetNode;

        while (currentNode != startNode) {
            path.Add(currentNode);
            currentNode = currentNode.parent;
        }
        Vector3[] waypoints = SimplifyPath(path);
        Array.Reverse(waypoints);
        return waypoints;
    }

    /// <summary>
    /// Simpifies the path produced by A* by removing redundant points in the path which don't
    /// alter traveling direction.
    /// </summary>
    /// <param name="path">List of nodes that form the path.</param>
    /// <returns>Vector3 primitive array of a simplified path.</returns>
    private Vector3[] SimplifyPath(List<Node> path) {
        List<Vector3> waypoints = new List<Vector3>();
        Vector2 directionOld = Vector2.zero;

        // Loop through the path and check the direction between points, adding only ones which change the traveling direction
        for (int i = 1; i < path.Count; ++i) {
            Vector2 directionNew = new Vector2(path[i - 1].gridX - path[i].gridX, path[i - 1].gridY - path[i].gridY);
            if (directionNew != directionOld) {
                waypoints.Add(path[i].worldPosition);
            }
            directionOld = directionNew;
        }
        return waypoints.ToArray();
    }

    /// <summary>
    /// Get the euclidean distance between two nodes nodes
    /// </summary>
    /// <param name="nodeA"></param>
    /// <param name="nodeB"></param>
    /// <returns>Amount of nodes between nodeA and nodeB</returns>
    private int GetDistance(Node nodeA, Node nodeB) {
        int dstX = Mathf.Abs(nodeA.gridX - nodeB.gridX);
        int dstY = Mathf.Abs(nodeA.gridY - nodeB.gridY);

        // Using right angle triangles to calculate diagonal distance (multiplied by 10 for ease of use)
        if (dstX > dstY) return 14 * dstY + 10 * (dstX - dstY);
        return 14 * dstX + 10 * (dstY - dstX);
    }

    /// <summary>
    /// Draws debug information (only visible in the Editor).
    /// </summary>
    private void OnDrawGizmos() {
        if (!drawGizmoEnd) return;
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(endLocation, 0.25f);
    }
}
