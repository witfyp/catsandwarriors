﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class <c>JsonHelper</c> is helper class for reading and writing jagged arrays to Json.
/// 
/// <para>
/// AUTHOR: ffleurey
/// ,see <a href="https://forum.unity.com/threads/how-to-load-an-array-with-jsonutility.375735/">link</a>
/// </para>
/// </summary>
public class JsonHelper {
    public static T[] getJsonArray<T>(string json) {
        string newJson = "{ \"array\": " + json + "}";
        Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(newJson);
        return wrapper.array;
    }

    [System.Serializable]
    private class Wrapper<T> {
        public T[] array;
    }
}
