﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

/// <summary>
/// <para>Class <c>FSM</c> creates a finite state machine structure.</para>
/// 
/// <para>
/// AUTHOR : Brent Owens 
/// ,see <a href="https://github.com/sploreg/goap">link</a>.
/// </para>
/// </summary>
public class FSM {

    private Stack<FSMState> stateStack = new Stack<FSMState>();

    public delegate void FSMState(FSM fsm, GameObject gameObject);

    public void Update(GameObject gameObject) {
        if (stateStack.Peek() != null) {
            stateStack.Peek().Invoke(this, gameObject);
        }
    }
	
    public void PushState(FSMState state) {
        stateStack.Push(state);
    }

    public void PopState() {
        stateStack.Pop();
    }

    public void Clear() {
        stateStack.Clear();
    }

    public FSMState PeekState() {
        return stateStack.Peek();
    }
}
