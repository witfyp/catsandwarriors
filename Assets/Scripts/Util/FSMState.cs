﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// <para>Interface <c>FSMState</c> is an interface for creating a state for <see cref="FSM"/>.</para>
/// 
/// <para>
/// AUTHOR : Brent Owens 
/// ,see <a href="https://github.com/sploreg/goap">link</a>.
/// </para>
/// </summary>
public interface FSMState {

    void Update(FSM fsm, GameObject gameObject);
}
