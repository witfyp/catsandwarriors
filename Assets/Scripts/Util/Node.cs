﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// <para>Class <c>Node</c> is a node class used for the pathfinding grid.</para>
/// 
/// <para>
/// AUTHOR : Sebastian Lague 
/// ,see <a href="https://github.com/SebLague/Pathfinding">link</a>.
/// </para>
/// </summary>
public class Node : IHeapItem<Node>{

    public bool walkable;
    public Vector2 worldPosition;
    public int gridX;
    public int gridY;
    public int cost;

    public int gCost;
    public int hCost;

    public Node parent;
    int heapIndex;

    public Node(bool walkable, Vector3 worldPosition, int gridX, int gridY, int cost) {
        this.walkable = walkable;
        this.worldPosition = worldPosition;
        this.gridX = gridX;
        this.gridY = gridY;
        this.cost = cost;
    }

    public int fCost {
        get {
            return gCost + hCost;
        }
    }

    public int HeapIndex {
        get {
            return heapIndex;
        }
        set {
            heapIndex = value;
        }
    }

    public int CompareTo(Node nodeToCompare) {
        int compare = fCost.CompareTo(nodeToCompare.fCost);
        if (compare == 0) {
            compare = hCost.CompareTo(nodeToCompare.hCost);
        }
        return -compare;
    }
}
