﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum QuestType { KILL, GATHER, COUNT };

public class Quest : MonoBehaviour {

    [SerializeField]
    private bool useRandom;
    [SerializeField]
    private int maxAmount = 3;
    [SerializeField]
    private int minAmount = 1;
    [SerializeField]
    private List<ItemBase> possibleGatherItems;
    [SerializeField]
    private ItemBase gatherItem;
    [SerializeField]
    private QuestType type;
    [SerializeField]
    private int amount;
    [SerializeField]
    private ItemBase reward;
    [SerializeField]
    private string killTargetTag;
    [SerializeField]
    [TextArea(3, 10)]
    private string questDescription;

    private Character character;

    [SerializeField]
    private int completedAmount = 0;

    public string Description { get { return questDescription; } }

    public ItemBase Reward { get { return reward; } }

    public void Init() {
        if (!useRandom) return;
        while ((possibleGatherItems == null || possibleGatherItems.Count == 0) && type == QuestType.GATHER)
            type = (QuestType)Random.Range(0, (int)QuestType.COUNT);
        amount = Random.Range(minAmount, maxAmount);
        if (possibleGatherItems != null && possibleGatherItems.Count > 0) gatherItem = possibleGatherItems[Random.Range(0, possibleGatherItems.Count)];

        GenerateQuestDescription();
    }

    public void Copy(Quest quest) {
        character = GetComponent<Character>();
        type = quest.type;
        gatherItem = quest.gatherItem;
        amount = quest.amount;
        reward = quest.reward;
        killTargetTag = quest.killTargetTag;
        if (reward == null) reward = Database.Item.FindItem(ItemType.HEALTH_POTION);
        questDescription = quest.Description;
    }

    public void CountKill(string tag) {
        if (string.IsNullOrEmpty(tag)) return;
        if (type != QuestType.KILL) return;
        if (killTargetTag == tag) completedAmount++;
    }

	public virtual bool IsDone() {
        switch (type) {
            case QuestType.KILL:
                return completedAmount >= amount;
            case QuestType.GATHER:
                if (character.Inventory == null) return false;
                return character.Inventory.GetItemCount(gatherItem.type) >= amount;
        }
        return false;
    }

    public virtual void Complete() {
        if (character.Inventory != null) {
            for (int i = 0; i < amount; ++i) character.Inventory.RemoveItem(gatherItem);
            character.Inventory.AddItem(reward);
        }
        if (character.Money != null) character.Money.IncreaseValue(Random.Range(30,60));
        Destroy(this);
    }

    private void GenerateQuestDescription() {
        switch (type) {
            case QuestType.KILL:
                questDescription = "Kill " + amount + " "+ killTargetTag + (amount>1? "s":"");
                break;
            case QuestType.GATHER:
                questDescription = "Gather " + amount + " \""+gatherItem.itemName+"\"";
                break;
        }
    }
}
