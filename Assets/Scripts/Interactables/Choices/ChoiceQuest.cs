﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChoiceQuest : ChoiceBase {

    [SerializeField]
    private int questIndex;

    [SerializeField]
    private bool useRandom;

    [SerializeField]
    [TextArea(3, 10)]
    protected string[] QuestStartMessages;

    [SerializeField]
    [TextArea(3, 10)]
    protected string[] QuestNotCompleteMessages;

    [SerializeField]
    [TextArea(3, 10)]
    protected string[] QuestCompleteMessages;

    private List<Quest> quests;

    private void Start() {
        quests = new List<Quest>(GetComponents<Quest>());
        if (quests == null || quests.Count == 0) {
            Debug.LogError("No Quests Found To Give!");
            enabled = false;
        }
    }

    public override void OnExecute(Character character) {
        if (signChoice == null) return;
        ChoiceBox.Hide();

        Quest quest = character.GetComponent<Quest>();

        if (quest == null) {
            AddQuest(character);
            signChoice.OnInteract(character, this, QuestStartMessages, character.GetComponent<Quest>().Description);
            return;
        }

        if (!quest.IsDone()) {
            signChoice.OnInteract(character, this, QuestNotCompleteMessages, quest.Description);
        } else {
            quest.Complete();
            signChoice.OnInteract(character, this, QuestCompleteMessages);
        }
    }

    protected void AddQuest(Character character) {
        Quest quest = character.gameObject.AddComponent<Quest>();
        Quest questToGive = useRandom ? quests[Random.Range(0, quests.Count)] : quests[questIndex];
        questToGive.Init();
        quest.Copy(questToGive);
    }
}
