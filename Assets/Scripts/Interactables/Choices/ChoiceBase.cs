﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(InteractableSign))]
public class ChoiceBase : MonoBehaviour {

    public string ChoiceName;

    protected InteractableSignChoice signChoice;

    private void Awake() {
        signChoice = GetComponent<InteractableSignChoice>();
    }

    virtual public void OnExecute(Character character) {
        Debug.LogWarning("OnExecute is not implemented!", gameObject);
    }
}
