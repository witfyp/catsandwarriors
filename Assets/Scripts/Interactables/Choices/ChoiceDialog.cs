﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChoiceDialog : ChoiceBase {

    // Inspector field
    [SerializeField]
    [TextArea(3, 10)]
    protected string[] Messages;

    public override void OnExecute(Character character) {
        if (signChoice == null) return;

        ChoiceBox.Hide();

        signChoice.OnInteract(character, this, Messages);
    }
}
