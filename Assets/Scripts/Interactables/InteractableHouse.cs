﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableHouse : InteractableBase {

    [SerializeField]
    private int capacity = 1;
    [SerializeField]
    private SpriteRenderer doorSpriteRenderer;
    [SerializeField]
    private Sprite openDoor;
    [SerializeField]
    private Sprite closedDoor;

    private int currentCount = 0;

    private void Start() {
        doorSpriteRenderer.sprite = openDoor;
    }

    public bool Available { get { return currentCount < capacity; } }

    public override bool OnInteract(Character character) {
        if (!Available) return false;
        currentCount++;
        character.Movement.Frozen = true;
        character.Movement.Collidable = false;

        //Disable all visuals (player went inside the house)
        character.gameObject.GetComponentInChildren<Canvas>().enabled = false;
        character.gameObject.GetComponentInChildren<SpriteRenderer>().enabled = false;

        if (!Available) doorSpriteRenderer.sprite = closedDoor;

        return true;
    }

    public override bool StopInteraction(Character character) {
        currentCount--;
        character.Movement.Frozen = false;
        character.Movement.Collidable = true;

        //Enable all visuals (player left the house)
        character.gameObject.GetComponentInChildren<Canvas>().enabled = true;
        character.gameObject.GetComponentInChildren<SpriteRenderer>().enabled = true;

        doorSpriteRenderer.sprite = openDoor;

        return true;
    }
}
