﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableBase : MonoBehaviour {

    virtual public bool OnInteract(Character character) {
        Debug.LogWarning("OnInteract is not implemented");
        return true;
    }

    virtual public bool StopInteraction(Character character) {
        Debug.LogWarning("StopInteraction is not implemented");
        return true;
    }
}
