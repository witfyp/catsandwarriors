﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class InteractableChatbot : InteractableBase {

    public SpeechBubble speechBubble;

    private string[] askingForName;
    private string[] iKnowSubstitutes;
    private string[] iDontKnowSubstitutes;

    private void Awake() {
        askingForName = JsonHelper.getJsonArray<string>(Resources.Load<TextAsset>("Chatbot/Rules/nameRules").text);
        iKnowSubstitutes = JsonHelper.getJsonArray<string>(Resources.Load<TextAsset>("Chatbot/Rules/iKnowRules").text);
        iDontKnowSubstitutes = JsonHelper.getJsonArray<string>(Resources.Load<TextAsset>("Chatbot/Rules/iDontKnowRules").text);
    }

    public override bool OnInteract(Character character) {
        character.Movement.Frozen = true;
        if (GetComponent<Character>() != null) GetComponent<Character>().Movement.Frozen = true;

        if (character.gameObject.tag == "Player") ChatInputBox.Show(ReceiveQueryCallback);
        return true;
    }

    public override bool StopInteraction(Character character) {
        character.Movement.Frozen = false;
        if (GetComponent<Character>() != null) GetComponent<Character>().Movement.Frozen = false;

        if (character.gameObject.tag == "Player") ChatInputBox.Hide();
        return true;
    }

    public void ReceiveQuery(string query, Action<string> callback) {
        query = query.ToLower();
        string earlyResponse;
        if (ApplyPreChatRules(query, out earlyResponse)) {
            speechBubble.Show(earlyResponse);
            return;
        }
        ChatbotRequestManager.Request(new ChatbotRequest(query, callback));
    }

    private void ReceiveQueryCallback(string query) {
        query = query.ToLower();
        string earlyResponse;
        if (ApplyPreChatRules(query, out earlyResponse)) {
            speechBubble.Show(earlyResponse);
            return;
        }

        ChatbotRequestManager.Request(new ChatbotRequest(query, ReceiveResponseCallback));
    }

    private void ReceiveResponseCallback(string response) {
        speechBubble.Show(ApplyPostChatRules(response));
    }

    public bool ApplyPreChatRules(string query, out string response) {

        if (GetComponent<Character>() == null) {
            response = query;
            return false;
        }

        if (query.Contains("your") && query.Contains("name")) {
            response = askingForName[UnityEngine.Random.Range(0, askingForName.Length)] + GetComponent<Character>().Name;
            return true;
        }

        response = query;
        return false;
    }

    public string ApplyPostChatRules(string response) {

        if (response == "i dont know") return iDontKnowSubstitutes[UnityEngine.Random.Range(0, iDontKnowSubstitutes.Length)];
        if (response == "i know") return iKnowSubstitutes[UnityEngine.Random.Range(0, iKnowSubstitutes.Length)];
        return response;
    }
}
