﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableSign : InteractableBase {

    // Inspector field
    [SerializeField]
    [TextArea(3, 10)]
    protected string[] Messages;

    protected Queue<string> messageQueue;
    protected Coroutine typeMessageCoroutine;

    private void Awake() {
        messageQueue = new Queue<string>();
        typeMessageCoroutine = null;
    }

    public override bool OnInteract(Character character) {
        if (Messages.Length == 0) return false;

        if (!DialogBox.IsVisible()) Init(character, Messages);

        DisplayNextMessage(character);
        return true;
    }

    protected virtual void Init(Character character, string[] messages, string extra="") {

        messageQueue.Clear();

        foreach (string message in messages) messageQueue.Enqueue(message);

        if (!string.IsNullOrEmpty(extra)) messageQueue.Enqueue(extra);

        character.Movement.Frozen = true;
        StartCoroutine(FreezeTimeRoutine());
    }

    protected virtual void DisplayNextMessage(Character character) {

        if (messageQueue.Count == 0) {
            if (typeMessageCoroutine != null) StopCoroutine(typeMessageCoroutine);
            EndInteraction(character);
            return;
        }

        // Type one character of the message per frame (nice effect)
        if (typeMessageCoroutine != null) StopCoroutine(typeMessageCoroutine);
        typeMessageCoroutine = StartCoroutine(TypeMessage(messageQueue.Dequeue()));
    }



    protected void EndInteraction(Character character) {
        if (typeMessageCoroutine != null) StopCoroutine(typeMessageCoroutine);
        typeMessageCoroutine = null;
        Time.timeScale = 1;
        character.Movement.Frozen = false;
        if (DialogBox.IsVisible()) DialogBox.Hide();
        if (ChoiceBox.IsVisible()) ChoiceBox.Hide();
    }

    protected IEnumerator FreezeTimeRoutine() {
        Time.timeScale = 0;
        yield return null;
    }

    protected IEnumerator TypeMessage(string message) {

        DialogBox.Show("");

        foreach (char c in message) {
            DialogBox.Show(DialogBox.GetText() + c);
            yield return null;
        }
    }
}
