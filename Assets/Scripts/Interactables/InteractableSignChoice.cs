﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableSignChoice : InteractableSign {

    // Inspector fields
    [SerializeField]
    private List<ChoiceBase> choices;

    public override bool OnInteract(Character character) {
        return base.OnInteract(character);
    }

    public void OnInteract(Character character, ChoiceBase choiceDialog, string[] newMessages, string extra="") {
        if (Messages.Length == 0) return;
        Init(character, newMessages, extra);
        DisplayNextMessage(character);
    }

    protected override void Init(Character character, string[] messages, string extra="") {
        base.Init(character, messages, extra);
    }

    protected override void DisplayNextMessage(Character character) {
        ChoiceBox.Hide();
        if (messageQueue.Count == 0) {
            if (!ChoiceBox.IsVisible()) ChoiceBox.Show(choices, character);
            return;
        }

        // Type one character of the message per frame (nice effect)
        if (typeMessageCoroutine != null) StopCoroutine(typeMessageCoroutine);
        typeMessageCoroutine = StartCoroutine(TypeMessage(messageQueue.Dequeue()));

    }
}
