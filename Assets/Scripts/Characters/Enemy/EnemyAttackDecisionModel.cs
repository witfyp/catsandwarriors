﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// <para>Class <c>EnemyAttackDecisionModel</c> implements the NeuralNetworkAgent for wolves in the game.</para>
/// 
/// <para>
/// AUTHOR: Oleksandr Kononov
/// </para>
/// </summary>
[RequireComponent(typeof(CharacterHealthStatModel))]
[RequireComponent(typeof(CharacterEnergyModel))]
[RequireComponent(typeof(CharacterAttackModel))]
public class EnemyAttackDecisionModel : NeuralNetworkAgent {

    // Possible action delegate functions that the enemy agent can perform
    public EnemyAction onAttack;
    public EnemyAction onDefend;
    public EnemyAction onRun;
    public EnemyAction onPursue;

    /// <summary>
    /// Implementation of <see cref="MakeDecision(Character)"/> for the wolves in the game.
    /// </summary>
    /// <param name="target">Target character.</param>
    public override void MakeDecision(Character target) {
        // Safety checks
        if (network == null) return;
        if (target == null) return;
        if (target.Health == null) return;
        if (Busy) return;

        // Normalize Inputs
        float health = Normalize(character.Health.Value, 0, character.Health.MaxValue);
        float stamina = Normalize(character.Energy.Value, 0, character.Energy.MaxValue);
        float damage = character.Attack.Damage / (1 + character.Attack.Damage);
        float defence = character.Defend.Defence / (1 + character.Defend.Defence);
        float targetDistance = Vector2.Distance(transform.position, target.transform.position);
        targetDistance = targetDistance / (1 + targetDistance);
        float targetHealth = target.Health != null ? Normalize(target.Health.Value, 0, target.Health.MaxValue) : 0;
        float targetDamage = target.Attack != null ? (target.Attack.Damage / (1 + target.Attack.Damage)) : 0;
        int isAttacking = target.Attack != null ? (target.Attack.IsAttacking ? 1 : 0) : 0;
        int isDefending = target.Defend != null ? (target.Defend.IsDefending ? 1 : 0) : 0;
        int isFacingTarget = character.Movement.Facing(target.transform.position) ? 1 : 0;

        // Create inputs for input layer of the genetic neural network
        float[] inputs = {
            health,
            stamina,
            damage,
            defence,
            targetDistance,
            targetHealth,
            targetDamage,
            isAttacking,
            isDefending,
            isFacingTarget
        };

        // Get the outputs of the genetic neural network
        float[] outputs = network.FeedForward(inputs);

        Busy = true;
        lastAction = GetEnemyAttackAction(outputs);

        // Invoke the delegate action based on the output decision
        switch (lastAction) {
            case EnemyAttackAction.ATTACK:
                if (onAttack != null) onAttack.Invoke(target);
                break;
            case EnemyAttackAction.DEFEND:
                if (onDefend != null) onDefend.Invoke(target);
                break;
            case EnemyAttackAction.RUN:
                if (onRun != null) onRun.Invoke(target);
                break;
            case EnemyAttackAction.PURSUE:
                if (onPursue != null) onPursue.Invoke(target);
                break;
        }
    }

    /// <summary>
    /// Implementation of <see cref="CalculateFitness(Character)"/> for wolves.
    /// </summary>
    /// <param name="target">Target character.</param>
    public override void CalculateFitness(Character target) {
        // Ignore fitness if we're not training
        if (gameObject == null || PlayerPrefs.GetInt("GeneticAlgorithm", 0) == 0) return;
        
        // Check for targets death
        if (target == null || target.gameObject == null) {
            if (lastAction == EnemyAttackAction.ATTACK) network.AddFitness(200); //Killed Enemy - 200 fitness
            return;
        }

        // How close to the target am I
        float distance = Vector2.Distance(transform.position, target.transform.position);
        network.AddFitness(character.Movement.IsMoving ? (1 / distance) : 0);

        // EnemyHealth% - TargetHealth%
        float myHealthScore = Normalize(character.Health.Value, 0, character.Health.MaxValue);
        float targetHealthScore = Normalize(target.Health.Value, 0, target.Health.MaxValue);
        network.AddFitness(myHealthScore - targetHealthScore);

        // How long will the enemy last while in combat
        if (character.Health.Value < character.Health.MaxValue) network.AddFitness(Time.deltaTime * 0.5f);

        // How well I'm managing stamina
        float myEnergyScore = Normalize(character.Energy.Value, 0, character.Energy.MaxValue);
        network.AddFitness(myEnergyScore * 0.25f);
    }

    /// <summary>
    /// Get the final decision action from the outputs of the genetic neural network.
    /// </summary>
    /// <param name="outputs">Raw outputs of genetic neural network.</param>
    /// <returns></returns>
    protected override EnemyAttackAction GetEnemyAttackAction(float[] outputs) {
        int highestOutputIndex = 0;
        outputs = Softmax(outputs);
        for (int i = 0; i < outputs.Length; ++i) if (outputs[i] > outputs[highestOutputIndex]) highestOutputIndex = i;
        return (EnemyAttackAction)highestOutputIndex;
    }
}
