﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// <para>Class <c>EnemyAttackDecisionView</c> is a view in MVC structure. It's purpose is to display enemy
/// genetic neural network decision visually.</para>
/// 
/// <para>
/// AUTHOR: Oleksandr Kononov
/// </para>
/// </summary>
[RequireComponent(typeof(EnemyAttackDecisionModel))]
public class EnemyAttackDecisionView : MonoBehaviour {

    // Reference to an image container above the enemy head
    [SerializeField]
    private Image uiImage;

    // Sprites for each decision
    [SerializeField]
    private Sprite onAttack, onDefend, onPursue, onRun;

    // Current sprite
    private Sprite currentSprite;

    // Reference to the decision model of this agent
    private EnemyAttackDecisionModel attackDecision;

    /// <summary>
    /// Awake runs once at the initialisation of the scene stage and
    /// before any other functions (with the exception of other Awake functions).
    /// 
    /// <para>Used to get the reference to the decision model of the agent.</para>
    /// </summary>
    private void Awake() {
        attackDecision = GetComponent<EnemyAttackDecisionModel>();
    }

    /// <summary>
    /// Start function is the first after Awake functions.
    /// It is used to add it's own delegate to the decision models delegate functions to change the sprite image."/>.
    /// </summary>
    private void Start() {
        if (uiImage == null) {
            Debug.Log("No UI Image found!");
            enabled = false;
        }

        // Initialise the delegates
        uiImage.enabled = false;
        attackDecision.onAttack += delegate { currentSprite = onAttack; } ;
        attackDecision.onDefend += delegate { currentSprite = onDefend; };
        attackDecision.onPursue += delegate { currentSprite = onPursue; };
        attackDecision.onRun += delegate { currentSprite = onRun; };
    }

    /// <summary>
    /// Handle displaying the sprites above the head.
    /// </summary>
    private void Update() {
        uiImage.enabled = attackDecision.Active;
        uiImage.sprite = currentSprite;
    }
}
