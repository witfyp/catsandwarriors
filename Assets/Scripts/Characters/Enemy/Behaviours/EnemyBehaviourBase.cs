﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// <para>Abstract class <c>EnemyBehaviourBase</c> is used to create performable actions class
/// for neural network agents.</para>
/// 
/// <para>
/// AUTHOR: Oleksandr Kononov
/// </para>
/// </summary>
[RequireComponent(typeof(Character))]
[RequireComponent(typeof(CharacterEnergyModel))]
[RequireComponent(typeof(EnemyAttackDecisionModel))]
public abstract class EnemyBehaviourBase : MonoBehaviour {

    [SerializeField]
    protected float energyCost = 2f;
    [SerializeField]
    protected float duration = 1f;

    protected bool isExecuting = false;

    protected EnemyAttackDecisionModel decisionModel;
    protected Character character;

    /// <summary>
    /// Awake runs once at the initialisation of the scene stage and
    /// before any other functions (with the exception of other Awake functions).
    /// 
    /// <para>Used for initialisation of decisionModel and character.</para>
    /// </summary>
    private void Awake() {
        decisionModel = GetComponent<EnemyAttackDecisionModel>();
        character = GetComponent<Character>();
    }

    /// <summary>
    /// Virtual function for executing a decision.
    /// </summary>
    /// <param name="target">Target character.</param>
    public virtual void ExecuteDecision(Character target) {}

    /// <summary>
    /// Stop all coroutines when killed.
    /// </summary>
    public void OnDestroy() {
        StopAllCoroutines();
    }
}
