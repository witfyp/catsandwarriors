﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// <para>Abstract class <c>EnemyAttackBehaviour</c> is an implementation of <see cref="EnemyBehaviourBase"/>
/// for attack behaviour.</para>
/// 
/// <para>
/// AUTHOR: Oleksandr Kononov
/// </para>
/// </summary>
[RequireComponent(typeof(CharacterAttackModel))]
[RequireComponent(typeof(CharacterMovementModel))]
public class EnemyAttackBehaviour : EnemyBehaviourBase {

    /// <summary>
    /// Start function is the first after Awake functions.
    /// It is used to the set delegate for onAttack to <see cref="ExecuteAttack(Character)"/>.
    /// </summary>
    private void Start() {
        decisionModel.onAttack += ExecuteDecision;
    }

    /// <summary>
    /// Executes the attack behaviour.
    /// </summary>
    /// <param name="target">Target character.</param>
    public override void ExecuteDecision(Character target) {
        if (character.Energy.Value <= 0) return;
        if (character.Attack.IsAttacking || character.Movement.Frozen) return;

        if (!isExecuting) StartCoroutine(ExecuteAttack(target));
    }

    /// <summary>
    /// Performs an attack.
    /// </summary>
    /// <param name="target">Target character.</param>
    /// <returns></returns>
    private IEnumerator ExecuteAttack(Character target) {
        //Debug.Log("Attacked");
        isExecuting = true;
        character.Attack.BeginAttack();

        while (character.Attack.IsAttacking) yield return null;
        character.Energy.DecreaseValue(energyCost);
        decisionModel.CalculateFitness(target);
        yield return new WaitForSeconds(duration);
        decisionModel.Busy = false;
        isExecuting = false;
    }
}
