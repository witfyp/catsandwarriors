﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// <para>Abstract class <c>EnemyPursueBehaviour</c> is an implementation of <see cref="EnemyBehaviourBase"/>
/// for pursue behaviour.</para>
/// 
/// <para>
/// AUTHOR: Oleksandr Kononov
/// </para>
/// </summary>
[RequireComponent(typeof(CharacterMovementModel))]
[RequireComponent(typeof(PathfindingAgent))]
public class EnemyPursueBehaviour : EnemyBehaviourBase {

    // Refence to the pathfinding agent component.
    private PathfindingAgent pathfindingAgent;

    /// <summary>
    /// Start function is the first after Awake functions.
    /// It is used to the set delegate for onPuruse to <see cref="ExecuteAttack(Character)"/>.
    /// </summary>
    private void Start() {
        pathfindingAgent = GetComponent<PathfindingAgent>();
        decisionModel.onPursue += ExecuteDecision;
    }

    /// <summary>
    /// Executes a puruse behaviour.
    /// </summary>
    /// <param name="target">Target character.</param>
    public override void ExecuteDecision(Character target) {
        if (character.Energy.Value <= 0) return;
        if (character.Movement.Frozen) return;

        if (!isExecuting) StartCoroutine(ExecutePursue(target));
    }

    /// <summary>
    /// Pursues the target with A*.
    /// </summary>
    /// <param name="target">Target character.</param>
    /// <returns></returns>
    private IEnumerator ExecutePursue(Character target) {
        isExecuting = true;

        pathfindingAgent.StopPathfinding();
        pathfindingAgent.StartPathfinding(target.gameObject);

        while (!pathfindingAgent.Done) yield return null;

        character.Energy.DecreaseValue(energyCost);
        pathfindingAgent.StopPathfinding();
        decisionModel.CalculateFitness(target);
        decisionModel.Busy = false;
        isExecuting = false;
    }
}
