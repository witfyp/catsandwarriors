﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// <para>Abstract class <c>EnemyDefendBehaviour</c> is an implementation of <see cref="EnemyBehaviourBase"/>
/// for defend behaviour.</para>
/// 
/// <para>
/// AUTHOR: Oleksandr Kononov
/// </para>
/// </summary>
[RequireComponent(typeof(CharacterDefendModel))]
public class EnemyDefendBehaviour : EnemyBehaviourBase {

    /// <summary>
    /// Start function is the first after Awake functions.
    /// It is used to the set delegate for onDefend to <see cref="ExecuteAttack(Character)"/>.
    /// </summary>
    private void Start() {
        decisionModel.onDefend += ExecuteDecision;
    }

    /// <summary>
    /// Executes a defence behaviour.
    /// </summary>
    /// <param name="target">Target character.</param>
    public override void ExecuteDecision(Character target) {
        if (character.Energy.Value <= 0) return;
        if (character.Attack != null && character.Attack.IsAttacking) return;

        if (!isExecuting) StartCoroutine(ExecuteDefend(target));
    }

    /// <summary>
    /// Performs a defence.
    /// </summary>
    /// <param name="target">Target character.</param>
    /// <returns></returns>
    private IEnumerator ExecuteDefend(Character target) {
        //Debug.Log("Defending");
        isExecuting = true;
        character.Defend.Defend(true);

        character.Energy.DecreaseValue(energyCost);
        decisionModel.CalculateFitness(target);
        character.Defend.Defend(false);
        yield return new WaitForSeconds(duration);
        decisionModel.Busy = false;
        isExecuting = false;
    }
}
