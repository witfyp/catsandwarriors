﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// <para>Abstract class <c>EnemyRunBehaviour</c> is an implementation of <see cref="EnemyBehaviourBase"/>
/// for run (flee) behaviour.</para>
/// 
/// <para>
/// AUTHOR: Oleksandr Kononov
/// </para>
/// </summary>
[RequireComponent(typeof(CharacterMovementModel))]
[RequireComponent(typeof(PathfindingAgent))]
public class EnemyRunBehaviour : EnemyBehaviourBase {

    [SerializeField]
    private float runRange = 3f;

    // Refence to the pathfinding agent component.
    private PathfindingAgent pathfindingAgent;

    /// <summary>
    /// Start function is the first after Awake functions.
    /// It is used to the set delegate for onRun to <see cref="ExecuteAttack(Character)"/>.
    /// </summary>
    private void Start() { 
        pathfindingAgent = GetComponent<PathfindingAgent>();
        decisionModel.onRun += ExecuteDecision;
    }

    /// <summary>
    /// Executes a run behaviour.
    /// </summary>
    /// <param name="target">Target character.</param>
    public override void ExecuteDecision(Character target) {
        if (character.Energy.Value <= 0) return;
        if (character.Movement.Frozen) return;
        if (Vector2.Distance(transform.position, target.transform.position) >= 10f) return;

        if (!isExecuting) {
            pathfindingAgent.StopPathfinding();
            StartCoroutine(ExecuteRun(target));
        }
    }

    /// <summary>
    /// Runs (flees) from the target with A*.
    /// </summary>
    /// <param name="target">Target character.</param>
    /// <returns></returns>
    private IEnumerator ExecuteRun(Character target) {
        isExecuting = true;

        int dir = transform.position.x < target.transform.position.x ? -1 : 1;
        float xMin = transform.position.x + (runRange * dir);
        float xMax = transform.position.x + dir;
        float yMin = transform.position.y - runRange;
        float yMax = transform.position.y + runRange;
        Vector2 runLocation = new Vector2(Random.Range(xMin, xMax), Random.Range(yMin, yMax));

        while (!PathRequestManager.IsWalkable(gameObject, runLocation))
            runLocation = new Vector2(Random.Range(xMin, xMax) * dir, Random.Range(yMin, yMax));

        pathfindingAgent.StartPathfinding(runLocation);

        while (!pathfindingAgent.Done) yield return null;

        character.Energy.DecreaseValue(energyCost);
        pathfindingAgent.StopPathfinding();
        yield return new WaitForSeconds(duration);
        decisionModel.Busy = false;
        isExecuting = false;
    }
}
