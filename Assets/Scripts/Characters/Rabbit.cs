﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rabbit : MonoBehaviour {

    public delegate void OnDeath();
    public OnDeath onDeathCallback;

    private void OnDestroy() {
        if (onDeathCallback != null) onDeathCallback.Invoke();
    }
}
