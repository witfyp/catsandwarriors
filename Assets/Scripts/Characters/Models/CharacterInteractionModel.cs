﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Character))]
public class CharacterInteractionModel : MonoBehaviour {

    private Character character;
    private CharacterMovementModel movementModel;
    private InteractableBase currentInteractable;

    private void Awake() {
        character = GetComponent<Character>();
        movementModel = GetComponent<CharacterMovementModel>();
        currentInteractable = null;
    }

    public void OnInteract() {
        InteractableBase usableInteractable = FindUsableInteractable();

        if (usableInteractable == null) return;

        currentInteractable = usableInteractable;
        usableInteractable.OnInteract(character);
    }

    public void OnEndInteraction() {
        if (currentInteractable != null) currentInteractable.StopInteraction(character);
        currentInteractable = null;
    }

    private InteractableBase FindUsableInteractable() {
        InteractableBase closestInteractable = null;
        foreach (Collider2D closeCollider in Physics2D.OverlapCircleAll(transform.position, 1f)) {
            if (closeCollider.gameObject == gameObject) continue;
            InteractableBase colliderInteractable = closeCollider.GetComponent<InteractableBase>();
            if (colliderInteractable == null) continue;

            if (closestInteractable == null) closestInteractable = colliderInteractable;
            else {
                if (Vector2.Distance(transform.position, colliderInteractable.transform.position) < Vector2.Distance(transform.position, closestInteractable.transform.position))
                    closestInteractable = colliderInteractable;
            }
        }

        return closestInteractable;
    }
}
