﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterToolModel : MonoBehaviour {

    private ItemTool tool;
    private float toolDurability;

    public float Durability { get { return toolDurability; } }
    public bool HasTool { get { return tool != null; } }
    public ItemTool Tool { get { return tool; } }

    public void DecreaseToolDurability(float amount) {
        toolDurability -= amount;
        if (toolDurability > 0) return;
        toolDurability = 0;
        tool = null;
    }

    public void GiveTool(ItemTool tool) {
        this.tool = tool;
        toolDurability = tool.durability;
    }

}
