﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovementModel : MonoBehaviour {

    [SerializeField]
    private float speed = 2f;
    private float speedMultiplier = 1f;
    private float speedMultiplerTimer = 0f;

    private Vector2 movementDirection;
    private bool facingLeft;
    private Rigidbody2D body;

    private bool isFrozen;
    private bool isCollidable;

    public delegate void OnSpeedBoost();
    public OnSpeedBoost onSpeedBoostCallback;

    private void Awake() {
        body = GetComponent<Rigidbody2D>();
        isCollidable = true;
    }

    private void FixedUpdate() {
        UpdateSpeedMultiplier();
        UpdateMovement();
	}

    private void UpdateSpeedMultiplier() {
        if (speedMultiplerTimer <= 0f) {
            float previousMultiplier = speedMultiplier;
            speedMultiplier = 1f;
            if (previousMultiplier != 1f && onSpeedBoostCallback != null) onSpeedBoostCallback.Invoke();
            return;
        }
        speedMultiplerTimer -= Time.deltaTime;
    }

    private void UpdateMovement() {
        if (isFrozen) {
            body.velocity = Vector2.zero;
            return;
        }

        if (movementDirection != Vector2.zero) movementDirection.Normalize();
        Vector2 currentPosition = new Vector2(transform.position.x, transform.position.y);
        body.MovePosition(currentPosition + movementDirection * Speed * Time.deltaTime);
    }

    public void SetFacingDirection(bool facingLeft) {
        this.facingLeft = facingLeft;
    }
    
    public void SetDirection(Vector2 direction) {
        if (Frozen) return;
        movementDirection = new Vector3(direction.x, direction.y, 0);
        if (movementDirection.x == 0) return;
        facingLeft = movementDirection.x < 0;
    }

    public void SetSpeedMultiplier(float multiplier, float duration) {
        speedMultiplier = multiplier;
        speedMultiplerTimer = duration;
        if (onSpeedBoostCallback != null) onSpeedBoostCallback.Invoke();
    }

    public bool Facing(Vector3 position) {
        if (position.x > transform.position.x) return FacingLeft;
        else return !FacingLeft;
    }

    public bool SpeedBoost { get { return speedMultiplier > 1; } }

    public bool FacingLeft { get { return facingLeft; } }

    public Vector2 MovementDirection { get { return movementDirection; } }

    public bool Frozen { get { return isFrozen; } set { isFrozen = value; } }

    public bool Collidable {
        get { return isCollidable; }
        set {
            isCollidable = value;
            GetComponent<Collider2D>().isTrigger = !isCollidable;
        }
    }

    public float Speed { get { return speed * speedMultiplier; } }

    public bool IsMoving { get { return !Frozen && movementDirection != Vector2.zero; } }
}
