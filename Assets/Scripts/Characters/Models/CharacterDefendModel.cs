﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterMovementModel))]
public class CharacterDefendModel : MonoBehaviour {

    [SerializeField]
    private float defence = 2f;

    private bool isDefending = false;

    private float defenceMultiplier = 1f;
    private float defenceMultiplierTimer = 0f;

    private CharacterMovementModel movement;

    public float Defence { get { return defence; } }
    public bool IsDefending { get { return isDefending; } }

    private void Awake() {
        movement = GetComponent<CharacterMovementModel>();
    }

    public void Defend(bool defend) {
        if (movement.Frozen && !isDefending) return;
        if (GetComponent<CharacterEnergyModel>() != null && GetComponent<CharacterEnergyModel>().Value <= 0) return;
        isDefending = defend;
        movement.Frozen = IsDefending;
    }
}
