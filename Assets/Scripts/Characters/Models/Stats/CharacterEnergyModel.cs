﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterEnergyModel : CharacterStatModelBase {

    [SerializeField]
    private float restorationRate = 0f;
    [SerializeField]
    private float restorationTimer = 0f;

    private float energyRestorationTimer = 0f;

	// Update is called once per frame
	public override void Update () {
        base.Update();
        if (restorationRate == 0) return;
        energyRestorationTimer += Time.deltaTime;
        if (energyRestorationTimer >= restorationTimer) {
            energyRestorationTimer = 0f;
            IncreaseValue(restorationRate);
        }
	}
}
