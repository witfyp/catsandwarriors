﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CharacterStatModelBase : MonoBehaviour {

    [SerializeField]
    protected float maxValue = 100f;
    [SerializeField]
    private float minValue = 50f;
    [SerializeField]
    private bool useRandom = true;
    [SerializeField]
    protected float depletionRate = 0.5f;
    [SerializeField]
    protected float depletionTimer = 10f;
    [SerializeField]
    [Range(1, 100)]
    private float lowPointPercentage = 30f;

    private float timer;
    protected float value;

    public delegate void OnStatChanged();
    public OnStatChanged onStatChangedCallback;

    public float LowPoint { get { return (maxValue / 100) * lowPointPercentage; } }
    public float MaxValue { get { return maxValue; } }
    public float Value { get { return value; } }

    public virtual void IncreaseValue(float amount) {
        value += amount;
        if (value > maxValue) value = maxValue;
        if (onStatChangedCallback != null) onStatChangedCallback.Invoke();
    }

    public virtual void DecreaseValue(float amount) {
        value -= amount;
        if (value < 0) value = 0;
        if (onStatChangedCallback != null) onStatChangedCallback.Invoke();
    }

    // Use this for initialization
    private void Start() {
        if (useRandom) maxValue = Random.Range(minValue, maxValue);
        value = maxValue;
        timer = 0;
        if (onStatChangedCallback != null) onStatChangedCallback.Invoke();
    }

    // Update is called once per frame
    public virtual void Update() {
        // You can't be frozen and starve to death - i.e. during chatbot interaction
        if (depletionRate == 0) return;
        if (GetComponent<CharacterMovementModel>() != null && GetComponent<CharacterMovementModel>().Frozen) return;

        timer += Time.deltaTime;
        if (timer >= depletionTimer) {
            timer = 0;
            DecreaseValue(depletionRate);
        }
    }


}
