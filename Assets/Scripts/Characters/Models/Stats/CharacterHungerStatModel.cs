﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterHungerStatModel : CharacterStatModelBase {

    [SerializeField]
    private float starvationHealthDamage = 10f;
    [SerializeField]
    private float starvationHealthTimer = 0.5f;

    private Character character;
    private float starvationTimer = 0f;

    private void Awake() {
        character = GetComponent<Character>();
    }

    // Update is called once per frame
    public override void Update () {
        base.Update();
        if (Value > 0) return;
        starvationTimer += Time.deltaTime;
        if (starvationTimer >= starvationHealthTimer) {
            starvationTimer = 0;
            character.Health.DecreaseValue(starvationHealthDamage);
        }
	}
}
