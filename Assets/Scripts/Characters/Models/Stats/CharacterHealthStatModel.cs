﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterHealthStatModel : CharacterStatModelBase {

    public delegate void HealthEvent();
    public HealthEvent onDeath;

    [SerializeField]
    private GameObject onDeathReplaceWith;
    [SerializeField]
    private GameObject onDeathEffect;

    private Character character;
    private float healthHungerDamageTimer;

    public Character attackedBy;

    private void Awake() {
        character = GetComponent<Character>();
        attackedBy = null;
    }

    public void TakeDamage(Character from, float amount) {
        DecreaseValue(amount);
        attackedBy = from;
    }

    public override void DecreaseValue(float amount) {
        if (character.Defend != null) {
            if (character.Defend.IsDefending) amount -= character.Defend.Defence;
        }
        value -= amount;
        if (value < 0) value = 0;
        if (onStatChangedCallback != null) onStatChangedCallback.Invoke();
    }

    // Update is called once per frame
    public override void Update () {
        if (Value > 0) return;
        if (onDeathEffect != null) Destroy(Instantiate(onDeathEffect, transform.position, Quaternion.identity), 1f);
        if (onDeathReplaceWith != null) Instantiate(onDeathReplaceWith, transform.position, Quaternion.identity);
        if (character.Inventory != null) character.Inventory.DropAll();
        if (gameObject.tag == "Player") HandlePlayerDeath();
        Destroy(gameObject);
    }

    private void HandlePlayerDeath() {
        transform.Find("Main Camera").parent = null;
        GameObject canvas = GameObject.FindGameObjectWithTag("MainCanvas");
        if (canvas == null) return;
        for (int i = 0; i < canvas.transform.childCount; ++i) canvas.transform.GetChild(i).gameObject.SetActive(false);
        GameObject gameOver = canvas.transform.Find("GameOver").gameObject;
        if (gameOver == null) return;
        gameOver.SetActive(true);
    }

    private void OnDestroy() {
        if (onDeath != null && Value <= 0) onDeath.Invoke();
        if (attackedBy == null) return;
        if (attackedBy.Money != null) attackedBy.Money.IncreaseValue(Random.Range(5, 20));
        Quest quest = attackedBy.GetComponent<Quest>();
        if (quest == null) return;
        quest.CountKill(gameObject.tag);
    }
}
