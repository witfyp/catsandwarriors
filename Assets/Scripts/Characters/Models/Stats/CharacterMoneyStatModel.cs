﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMoneyStatModel : CharacterStatModelBase {

    public override void IncreaseValue(float amount) {
        value += amount;
        if (onStatChangedCallback != null) onStatChangedCallback.Invoke();
    }
}
