﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterMovementModel))]
[RequireComponent(typeof(Character))]
public class CharacterAttackModel : MonoBehaviour {

    [SerializeField]
    private float damage = 10f;
    [SerializeField]
    private float attackRange = 1f;
    [SerializeField]
    private bool drawGizmo = false;

    private bool isAttacking = false;

    private float attackMultiplier = 1f;
    private float attackMultiplerTimer = 0f;

    private const float attackRangeHeight = 2f;

    private CharacterMovementModel movement;

    public delegate void OnAttack();
    public OnAttack onAttackCallback;
    public OnAttack onAttackBoostedCallback;

    public float Damage { get { return damage * attackMultiplier; } }
    public bool IsAttacking { get { return isAttacking; } }

    private void Awake() {
        movement = GetComponent<CharacterMovementModel>();
    }

    private void Update() {
        if (attackMultiplerTimer <= 0f) {
            float previousAttackMultiplier = attackMultiplier;
            attackMultiplier = 1f;
            if (previousAttackMultiplier != 1f && onAttackBoostedCallback != null) onAttackBoostedCallback.Invoke();
            return;
        }
        attackMultiplerTimer -= Time.deltaTime;
    }

    public void BeginAttack() {
        if (movement.Frozen) return;
        if (GetComponent<CharacterEnergyModel>() != null && GetComponent<CharacterEnergyModel>().Value <= 0) return;
        isAttacking = true;
        movement.Frozen = true;
        StartCoroutine(DoAttack());
        if (onAttackCallback != null) onAttackCallback.Invoke();
    }

    public void EndAttack() {
        movement.Frozen = false;
        isAttacking = false;
    }

    private IEnumerator DoAttack() {
        // Wait before doing damage
        yield return new WaitForSeconds(0.01f);
        float rangePosX = movement.FacingLeft ? transform.position.x - attackRange/2f : transform.position.x + attackRange/2f;
        Vector2 boxCenter = new Vector2(rangePosX, transform.position.y);
        Vector2 boxSize = new Vector2(attackRange, attackRangeHeight);
        CharacterHealthStatModel closestHealthCharacter = FindClosestCharacter(Physics2D.OverlapBoxAll(boxCenter, boxSize, 0));
        if (closestHealthCharacter == null) yield break;
        closestHealthCharacter.TakeDamage(GetComponent<Character>(), damage * attackMultiplier);
    }

    private CharacterHealthStatModel FindClosestCharacter(Collider2D[] colliders) {
        CharacterHealthStatModel closestHealthCharacter = null;
        foreach (Collider2D collider in colliders) {
            if (collider.gameObject == gameObject) continue;
            CharacterHealthStatModel health = collider.GetComponent<CharacterHealthStatModel>();
            if (health == null) continue;
            if (closestHealthCharacter == null) {
                closestHealthCharacter = health;
                continue;
            }
            if (Vector2.Distance(transform.position, health.transform.position) < Vector2.Distance(transform.position, closestHealthCharacter.transform.position))
                closestHealthCharacter = health;
        }
        return closestHealthCharacter;
    }

    public void SetAttackMultiplier(float multiplier, float duration) {
        attackMultiplier = multiplier;
        attackMultiplerTimer = duration;
        if (onAttackBoostedCallback != null) onAttackBoostedCallback.Invoke();
    }

    public bool AttackBoost { get { return attackMultiplier > 1; } }

    private void OnDrawGizmos() {
        if (!drawGizmo || movement == null) return;
        float rangePosX = movement.FacingLeft ? transform.position.x - attackRange/2f : transform.position.x + attackRange/2f;
        Vector2 boxCenter = new Vector2(rangePosX, transform.position.y);
        Gizmos.color = new Color(0, 0, 1, 0.5f);
        Gizmos.DrawCube(boxCenter, new Vector2(attackRange, attackRangeHeight));
    }
}
