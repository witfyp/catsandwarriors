﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterInventoryModel : MonoBehaviour {

    [SerializeField]
    private int capacity = 3;

    [SerializeField]
    private ItemBase[] startingItems;

    [SerializeField]
    private bool useRandomStartingItems = true;

    [SerializeField]
    private int amountOfRandomStartingItems = 1;

    private List<ItemBase> items = new List<ItemBase>();

    private Character character;

    // Callback for any change in the inventory
    public delegate void OnItemChanged();
    public OnItemChanged onItemChangedCallback;

    public int Capacity { get { return capacity; } }
    public int Count { get { return items.Count; } }

    private void Awake() {
        character = GetComponent<Character>();
        if (character == null) {
            Debug.LogError("No Character Found For Inventory Model!");
        }
        Invoke("AddStartingOffItems", 0.1f);
    }

    private void AddStartingOffItems() {
        if (startingItems.Length == 0) return;
        if (!useRandomStartingItems) foreach (ItemBase item in startingItems) AddItem(item);
        else {
            for (int i = 0; i < amountOfRandomStartingItems; ++i) {
                AddItem(startingItems[Random.Range(0, startingItems.Length)]);
            }
        }
    }

    public ItemBase GetItem(int index) { return items[index]; }

    public bool HasItem(ItemType type) { return GetItemCount(type) > 0; }

    public int GetItemCount(ItemType type) {
        int count = 0;
        foreach (ItemBase item in items) if (item.type == type) count++;
        return count;
    }

    public bool AddItem(ItemBase item) {
        if (item == null || item.type == ItemType.NONE || items.Count >= capacity) return false;
        items.Add(item);
        if (onItemChangedCallback != null) onItemChangedCallback.Invoke();
        return true;
    }

    public bool RemoveItem(ItemBase item) {
        if (item == null) return false;
        if (!items.Remove(item)) return false;
        if (onItemChangedCallback != null) onItemChangedCallback.Invoke();
        return true;
    }

    public bool UseItem(ItemBase item) {
        if (item == null) return false;
        return item.Use(character);
    }

    public bool DropItem(ItemBase item) {
        if (item == null) return false;
        if (!RemoveItem(item)) return false;
        if (item.prefab == null) return false;
        Vector2 droppingPosition = new Vector2(transform.position.x, transform.position.y - 1f);
        Instantiate(item.prefab, droppingPosition, Quaternion.identity);
        return true;
    }

    public void DropAll() {
        foreach(ItemBase item in items) {
            if (item.prefab == null) continue;
            Vector2 pos = transform.position;
            pos += new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f));
            Instantiate(item.prefab, pos, Quaternion.identity);
        }
    }
}
