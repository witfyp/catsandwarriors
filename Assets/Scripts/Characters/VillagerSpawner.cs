﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VillagerSpawner : MonoBehaviour {

    [System.Serializable]
    private struct VillagerType {
        public bool isMale;
        public AnimationClip idleAnimation;
        public AnimationClip walkingAnimation;

        public VillagerType(bool isMale, AnimationClip idleAnimation, AnimationClip walkingAnimation) {
            this.isMale = isMale;
            this.idleAnimation = idleAnimation;
            this.walkingAnimation = walkingAnimation;
        }
    }

    [SerializeField]
    private VillagerType[] villagerTypes;
    [SerializeField]
    private GameObject villagerTemplate;
    [SerializeField]
    public Vector2 range;
    [SerializeField]
    private int amountToSpawn = 7;
    [SerializeField]
    private bool displayGridGizmos = false;
    [SerializeField]
    private Transform leftSpawner, rightSpawner;

    private string[] maleNames;
    private Animator tempAnimator;
    private AnimatorOverrideController tempController;

    private void Awake() {
        maleNames = JsonHelper.getJsonArray<string>(Resources.Load<TextAsset>("Names/Male").text);
    }

    private void Start() {

        Vector2 origin = transform.position;

        for (int i = 0; i < amountToSpawn; ++i) {
            Vector2 randomRange = new Vector2(Random.Range(-range.x/2, range.x/2), Random.Range(-range.y/2, range.y/2));
            SpawnVillager(origin + randomRange);
        }
    }

    private void OnVillagerDeath() {
        int toSpawn = amountToSpawn - GameObject.FindGameObjectsWithTag("Villager").Length;
        Transform location = GameObject.FindGameObjectWithTag("Player").transform.position.x > 0 ? leftSpawner : rightSpawner;
        for (int i = 0; i < toSpawn; ++i) SpawnVillager(location.position);
    }

    private void SpawnVillager(Vector2 position) {
        GameObject villager = Instantiate(villagerTemplate, position, Quaternion.identity);
        tempAnimator = villager.GetComponentInChildren<Animator>();
        tempController = new AnimatorOverrideController(tempAnimator.runtimeAnimatorController);
        tempAnimator.runtimeAnimatorController = tempController;

        VillagerType type = villagerTypes[Random.Range(0, villagerTypes.Length)];

        tempController["StratosIdleAnimation"] = type.idleAnimation;
        tempController["StratosWalkAnimation"] = type.walkingAnimation;
        villager.GetComponent<Character>().Name = maleNames[Random.Range(0, maleNames.Length)];
        villager.GetComponent<CharacterHealthStatModel>().onDeath += OnVillagerDeath;
    }

    private void OnDrawGizmos() {
        if (!displayGridGizmos) return;

        Gizmos.color = Color.white;
        Gizmos.DrawWireCube(transform.position, range);
    }
}
