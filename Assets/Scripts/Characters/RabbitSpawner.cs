﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RabbitSpawner : MonoBehaviour {

    [SerializeField]
    private GameObject prefab;
    [SerializeField]
    private int maxAmount = 6;
    [SerializeField]
    [Range(0, 5)]
    private float spawningRangeX, spawningRangeY;
    [SerializeField]
    private float spawningNewEntityTimer = 6f;

    private GameObject[] spawnLocations;

    // Use this for initialization
    private void Start () {
        spawnLocations = GameObject.FindGameObjectsWithTag("RabbitSpawn");
        if (spawnLocations == null || spawnLocations.Length == 0) {
            Debug.LogError("No RabbitSpawn locations tags found for rabbit spawning!");
            enabled = false;
        }
        if (prefab == null) {
            Debug.LogError("No EnemyPrefab!");
            enabled = false;
        }
        InitialSpawn();
    }

    private void InitialSpawn() {
        for(int i=0; i<maxAmount; ++i) {
            Spawn();
        }
    }

    private void Spawn() {
        Vector2 spawnPos = spawnLocations[Random.Range(0, spawnLocations.Length)].transform.position;
        spawnPos += new Vector2(Random.Range(-spawningRangeX, spawningRangeX), Random.Range(-spawningRangeY, spawningRangeY));
        Rabbit r = Instantiate(prefab, spawnPos, Quaternion.identity).GetComponent<Rabbit>();
        if (r != null) r.onDeathCallback += OnRabbitKilled;
    }

    private void OnRabbitKilled() {
        if (this != null) StartCoroutine(SpawnRabbit());
    }
	
	private IEnumerator SpawnRabbit() {
        yield return new WaitForSeconds(spawningNewEntityTimer);
        Spawn();
    }
}
