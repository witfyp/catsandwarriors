﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class GoapGoalEntry {
    public GoapCondition condition;
    public bool value;
    public bool interupt = false;
}

public class Villager : MonoBehaviour, IGoap {

    [SerializeField]
    private bool canIdle = true;
    [SerializeField]
    private GoapGoalEntry[] goals;
    private int goalIndex;
    private Dictionary<GoapCondition, object> currentGoal;

    private GoapAgent goalAgent;
    private PathfindingAgent pathfindingAgent;

    private CharacterHealthStatModel health;
    private CharacterHungerStatModel hunger;
    private CharacterEnergyModel energy;
    private CharacterSocialStatModel social;
    private CharacterMoneyStatModel money;
    private CharacterToolModel tool;

    private void Awake() {
        goalAgent = GetComponent<GoapAgent>();
        pathfindingAgent = GetComponent<PathfindingAgent>();
        health = GetComponent<CharacterHealthStatModel>();
        hunger = GetComponent<CharacterHungerStatModel>();
        energy = GetComponent<CharacterEnergyModel>();
        social = GetComponent<CharacterSocialStatModel>();
        money = GetComponent<CharacterMoneyStatModel>();
        tool = GetComponent<CharacterToolModel>();
    }

    private void Start() {
        goalIndex = 0;
    }

    private void Update() {
        bool interupt = false;
        foreach(GoapGoalEntry goal in goals) {
            if (!goal.interupt) continue;
            if ((bool)GetWorldState()[goal.condition] != goal.value) {
                if (currentGoal != null && currentGoal.ContainsKey(goal.condition)) break;
                interupt = true;
                break;
            }
        }
        if (interupt) {
            Debug.Log("Interupting Current Goal");
            if (pathfindingAgent != null) pathfindingAgent.StopPathfinding();
            goalAgent.CreateNewPlan();
        }
    }

    public Dictionary<GoapCondition, object> GetWorldState() {
        Dictionary<GoapCondition, object> worldState = new Dictionary<GoapCondition, object>();

        if (health != null) worldState.Add(GoapCondition.HAS_HEALTH, health.Value > health.LowPoint);
        if (health != null) worldState.Add(GoapCondition.ATTACKED, health.attackedBy != null);
        if (hunger !=null) worldState.Add(GoapCondition.HAS_FOOD, hunger.Value > hunger.LowPoint);
        if (energy != null) worldState.Add(GoapCondition.HAS_ENERGY, energy.Value > energy.LowPoint);
        if (social != null) worldState.Add(GoapCondition.IS_SOCIAL, social.Value > social.LowPoint);
        if (money != null) worldState.Add(GoapCondition.HAS_MONEY, money.Value > money.LowPoint);
        if (tool != null) worldState.Add(GoapCondition.TOOL, tool.Tool);
        return worldState;
    }

    public Dictionary<GoapCondition, object> CreateGoalState() {
        Dictionary<GoapCondition, object> goal = new Dictionary<GoapCondition, object>();

        if (goals.Length == 0) return goal;
        Dictionary<GoapCondition, object> currentState = GetWorldState();

        goalIndex = goals.Length;

        for (int i = 0; i < goals.Length; ++i) {
            if (!currentState.ContainsKey(goals[i].condition)) continue;
            if ((bool)currentState[goals[i].condition] != goals[i].value) {
                goalIndex = i;
                break;
            }
        }

        if (goalIndex != goals.Length) {
            goal.Add(goals[goalIndex].condition, goals[goalIndex].value);
        } else {
            if (canIdle) goal.Add(GoapCondition.IDLE, true);
        }

        foreach(GoapCondition g in goal.Keys) Debug.Log("Goal: " + g.ToString(), gameObject);
        return goal;
    }

    public void PlanFailed(Dictionary<GoapCondition, object> failedGoal) {
        if (goals.Length == 0) return;
        currentGoal = null;
        goalIndex = ++goalIndex % goals.Length;
    }

    public void PlanFound(Dictionary<GoapCondition, object> goal, Queue<GoapAction> actions) {
        if (goals.Length == 0) return;
        currentGoal = goal;

        Debug.Log("<color=green>Plan Found</color>");
    }

    public void ActionsFinished() {
        goalIndex = 0;
        currentGoal = null;
        Debug.Log("<color=blue>Actions completed</color>");
    }

    public void PlanAborted(GoapAction aborter) {
        // An action bailed out of the plan. State has been reset to plan again.
        // Take note of what happened and make sure if you run the same goal again
        // that it can succeed.
        if (goals.Length == 0) return;
        currentGoal = null;
        goalIndex = ++goalIndex % goals.Length;
        Debug.Log("<color=red>Plan Aborted</color> " + aborter.ToString());
    }

    public bool MoveAgent(GoapAction nextAction) {
        if (pathfindingAgent == null || nextAction == null) {
            Debug.Log("<color=red>No pathfinding agent!</color>");
            return false;
        }

        if (nextAction.Target != null) pathfindingAgent.StartPathfinding(nextAction.Target);
        else pathfindingAgent.StartPathfinding(nextAction.Location);

        if (pathfindingAgent.Done) {
            // we are at the target location, we are done
            nextAction.InRange = true;
            pathfindingAgent.StopPathfinding();
            return true;
        }
        return false;
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        if (collision.gameObject.tag != "Player")
            Physics2D.IgnoreCollision(collision.collider, GetComponent<Collider2D>());
    }
}
