﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Character : MonoBehaviour {

    [SerializeField]
    private string characterName = "Villager";
    [SerializeField]
    private Text nameText;

    private void Start() {
        UpdateUIName();
    }

    public string Name { get { return characterName; } set { characterName = value; UpdateUIName(); } }

    private void UpdateUIName() {
        if (nameText != null) nameText.text = Name;
    }

    public CharacterMovementModel Movement { get { return GetComponent<CharacterMovementModel>(); } }
    public CharacterMovementView MovementView { get { return GetComponent<CharacterMovementView>(); } }
    public CharacterInteractionModel Interaction { get { return GetComponent<CharacterInteractionModel>(); } }
    public CharacterAttackModel Attack { get { return GetComponent<CharacterAttackModel>(); } }
    public CharacterDefendModel Defend { get { return GetComponent<CharacterDefendModel>(); } }
    public CharacterInventoryModel Inventory { get { return GetComponent<CharacterInventoryModel>(); } }
    public CharacterToolModel Tool { get { return GetComponent<CharacterToolModel>(); } }
    public CharacterHealthStatModel Health { get { return GetComponent<CharacterHealthStatModel>(); } }
    public CharacterHungerStatModel Hunger { get { return GetComponent<CharacterHungerStatModel>(); } }
    public CharacterEnergyModel Energy { get { return GetComponent<CharacterEnergyModel>(); } }
    public CharacterSocialStatModel Social { get { return GetComponent<CharacterSocialStatModel>(); } }
    public CharacterMoneyStatModel Money { get { return GetComponent<CharacterMoneyStatModel>(); } }

}
