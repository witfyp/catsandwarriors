﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterMoneyView : MonoBehaviour {

    [SerializeField]
    private Text moneyText;

    private CharacterMoneyStatModel money;

    private void Awake() {
        money = GetComponent<CharacterMoneyStatModel>();
        if (money == null) {
            Debug.LogWarning("Character Money View Has No Character Money Model!");
            enabled = false;
        }
        money.onStatChangedCallback += UpdateMoney;
    }

    private void UpdateMoney () {
        moneyText.text = money.Value.ToString();
	}
}
