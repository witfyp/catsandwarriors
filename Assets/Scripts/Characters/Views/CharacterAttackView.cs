﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAttackView : MonoBehaviour {

    [SerializeField]
    private Color boostedColour;

    private Animator animator;
    private SpriteRenderer spriteRenderer;
    private CharacterAttackModel attack;

    private const int numberOfAttacks = 2;
    private int currentAttack;

    private void Awake() {
        attack = GetComponent<CharacterAttackModel>();
        animator = GetComponentInChildren<Animator>();
        spriteRenderer = GetComponentInChildren<SpriteRenderer>();

        if (attack == null) {
            Debug.LogError("Character Attack View Could Not Find Reference to Attack Model!");
            enabled = false;
        }

        if (animator == null) {
            Debug.LogError("Character Animator is not setup!");
            enabled = false;
        }

        if (spriteRenderer == null) {
            Debug.LogError("<color=red>Character Sprite Renderer is not setup!</color>");
            enabled = false;
        }

        attack.onAttackCallback += DoAttack;
        attack.onAttackBoostedCallback += OnBoosted;
    }

    private void Start() {
        currentAttack = 0;
    }

    private void DoAttack() {
        animator.SetTrigger("Attack");
        animator.SetInteger("AttackType", currentAttack);
        currentAttack = ++currentAttack % numberOfAttacks;
    }

    private void OnBoosted() {
        if (attack.AttackBoost) spriteRenderer.color = boostedColour;
        else spriteRenderer.color = Color.white;
    }
}
