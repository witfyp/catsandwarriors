﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovementView : MonoBehaviour {

    [SerializeField]
    private Color boostedColour;

    private Animator animator;
    private SpriteRenderer spriteRenderer;

    private CharacterMovementModel movementModel;

    private void Awake() {
        movementModel = GetComponent<CharacterMovementModel>();
        animator = GetComponentInChildren<Animator>();
        spriteRenderer = GetComponentInChildren<SpriteRenderer>();

        if(movementModel == null) {
            Debug.LogError("Movement View Has Not Reference To Movement Model!");
            enabled = false;
        }

        if (animator == null) {
            Debug.LogError("<color=red>Character Animator is not setup!</color>");
            enabled = false;
        }

        if (spriteRenderer == null) {
            Debug.LogError("<color=red>Character Sprite Renderer is not setup!</color>");
            enabled = false;
        }

        movementModel.onSpeedBoostCallback += OnBoosted;
    }

    private void Update() {
        UpdateDirection();
    }

    private void UpdateDirection() {
        if (movementModel == null) {
            Debug.LogError("<color=orange>Movement Model not found!</color>", gameObject);
            return;
        }

        float speed = movementModel.Speed;

        if (movementModel.Frozen) speed = 0f;

        if (movementModel.IsMoving) animator.SetFloat("Speed", speed * 0.6f);

        spriteRenderer.flipX = movementModel.FacingLeft;

        animator.SetBool("IsMoving", movementModel.IsMoving);
    }

    private void OnBoosted() {
        if (movementModel.SpeedBoost) spriteRenderer.color = boostedColour;
        else spriteRenderer.color = Color.white;
    }
}
