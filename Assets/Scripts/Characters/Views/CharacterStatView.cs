﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CharacterStatView : MonoBehaviour {

    [Serializable]
    private class CharacterStat {
        public CharacterStatModelBase stat;
        public RectTransform statBar;
    }

    [SerializeField]
    private CharacterStat[] stats;

    private void Awake() {
        foreach(CharacterStat stat in stats) stat.stat.onStatChangedCallback += UpdateStats;
    }

    // Update is called once per frame
    private void UpdateStats () {
		foreach(CharacterStat stat in stats) {
            float percentage = stat.stat.Value / stat.stat.MaxValue;
            Vector3 scale = stat.statBar.localScale;
            scale.x = percentage;
            stat.statBar.localScale = scale;
        }
	}
}
