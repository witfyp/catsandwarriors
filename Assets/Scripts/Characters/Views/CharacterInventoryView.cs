﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterInventoryView : MonoBehaviour {

    [SerializeField]
    private GameObject inventoryUI;
    [SerializeField]
    private GameObject slotPrefab;

    private InventorySlot[] slots;

    private CharacterInventoryModel inventory;

    private void Awake() {
        inventory = GetComponent<CharacterInventoryModel>();
        if (inventory == null) {
            Debug.LogError("Inventory View Has No Inventory Model!");
            enabled = false;
        }

        inventory.onItemChangedCallback += UpdateUI;
    }

    private void Start() {
        for (int i = 0; i < inventory.Capacity; ++i) {
            GameObject slot = Instantiate(slotPrefab, inventoryUI.transform, false);
            ClickableObjectHelper buttonClickEvent = slot.GetComponent<ClickableObjectHelper>();
            buttonClickEvent.onLeftClickEventCallback += OnLeftItemClick;
            buttonClickEvent.onRightClickEventCallback += OnRightItemClick;
        }
        slots = inventoryUI.transform.GetComponentsInChildren<InventorySlot>();
    }

    private void UpdateUI() {
        for(int i = 0; i < slots.Length; ++i) {
            if (i < inventory.Count) slots[i].AddItem(inventory.GetItem(i));
            else slots[i].ClearSlot();
        }
    }

    private void OnLeftItemClick(InventorySlot slot) {
        inventory.UseItem(slot.Item);
    }

    private void OnRightItemClick(InventorySlot slot) {
        inventory.DropItem(slot.Item);
    }
}
