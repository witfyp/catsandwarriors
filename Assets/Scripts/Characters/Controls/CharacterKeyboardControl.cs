﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterKeyboardControl: CharacterBaseControl {

    private void Update() {
        UpdateMenu();
        UpdateDirection();
        UpdateAttack();
        UpdateDefend();
        UpdateAction();
    }

    private void UpdateMenu() {
        if (Input.GetKeyDown(KeyCode.Escape)) OnMenuPressed();
    }

    private void UpdateAttack() {
        if (Input.GetKeyDown(KeyCode.Space)) OnAttackPressed();
    }

    private void UpdateDefend() {
        OnDefendPressed(Input.GetKey(KeyCode.LeftShift));
    }

    private void UpdateAction() {
        if (Input.GetKeyDown(KeyCode.E)) OnActionPressed();
        if (Input.GetKeyDown(KeyCode.Q)) OnStopActionPressed();
    }

    private void UpdateDirection() {
        Vector2 newDirection = Vector2.zero;

        if (Input.GetKey(KeyCode.W)) newDirection.y = 1;
        if (Input.GetKey(KeyCode.S)) newDirection.y = -1;
        if (Input.GetKey(KeyCode.A)) newDirection.x = -1;
        if (Input.GetKey(KeyCode.D)) newDirection.x = 1;

        SetDirection(newDirection);
    }
}
