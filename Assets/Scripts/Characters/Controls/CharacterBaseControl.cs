﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Character))]
public class CharacterBaseControl : MonoBehaviour {

    private Character character;

    private void Awake() {
        character = GetComponent<Character>();
    }

    protected void SetDirection(Vector2 direction) {
        if (character.Movement == null) {
            Debug.LogError("<color=orange>Movement Model not found!</color>", gameObject);
            return;
        }
        character.Movement.SetDirection(direction);
    }

    protected void OnActionPressed() {
        if (character.Interaction == null) return;
        character.Interaction.OnInteract();
    }

    protected void OnAttackPressed() {
        if (character.Attack == null) return;
        character.Attack.BeginAttack();
    }

    protected void OnDefendPressed(bool defend) {
        if (character.Defend == null) return;
        character.Defend.Defend(defend);
    }

    protected void OnStopActionPressed() {
        if (character.Interaction == null) return;
        character.Interaction.OnEndInteraction();
    }

    protected void OnMenuPressed() {
        GameMenu.OnMenuPressed();
    }
}
