
# Table of Contents

1.  [Requires:]
2.  [More Information:]

A 2D fantasy game with strong focus in AI which uses various algorithms and machine learning.


# Requires:

-   Python 3.6+ 64bit
-   TensorFlow
-   TensorLayer
-   NumPy
-   ZeroMQ
-   Unity Game Engine


# More Information:

All the details about this final year project can be found in the **GameDesign.pdf** file.

